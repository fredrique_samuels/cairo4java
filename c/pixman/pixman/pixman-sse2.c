/*
 * Copyright © 2008 Rodrigo Kumpera
 * Copyright © 2008 André Tupinambá
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Red Hat not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Red Hat makes no representations about the
 * suitability of this software for any purpose.  It is provided "as is"
 * without express or implied warranty.
 *
 * THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS
 * SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS, IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
 * OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 *
 * Author:  Rodrigo Kumpera (kumpera@gmail.com)
 *          André Tupinambá (andrelrt@gmail.com)
 *
 * Based on work by Owen Taylor and Søren Sandmann
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <xmmintrin.h> /* for _mm_shuffle_pi16 and _MM_SHUFFLE */
#include <emmintrin.h> /* for SSE2 intrinsics */
#include "pixman-private.h"
#include "pixman-combine32.h"
#include "pixman-fast-path.h"

static __m128i mask_0080;
static __m128i mask_00ff;
static __m128i mask_0101;
static __m128i mask_ffff;
static __m128i mask_ff000000;
static __m128i mask_alpha;

static __m128i mask_565_r;
static __m128i mask_565_g1, mask_565_g2;
static __m128i mask_565_b;
static __m128i mask_red;
static __m128i mask_green;
static __m128i mask_blue;

static __m128i mask_565_fix_rb;
static __m128i mask_565_fix_g;

static force_inline __m128i
unpack_32_1x128 (uint32_t data)
{
    return _mm_unpacklo_epi8 (_mm_cvtsi32_si128 (data), _mm_setzero_si128 ());
}

static force_inline void
unpack_128_2x128 (__m128i data, __m128i* data_lo, __m128i* data_hi)
{
    *data_lo = _mm_unpacklo_epi8 (data, _mm_setzero_si128 ());
    *data_hi = _mm_unpackhi_epi8 (data, _mm_setzero_si128 ());
}

static force_inline __m128i
unpack_565_to_8888 (__m128i lo)
{
    __m128i r, g, b, rb, t;

    r = _mm_and_si128 (_mm_slli_epi32 (lo, 8), mask_red);
    g = _mm_and_si128 (_mm_slli_epi32 (lo, 5), mask_green);
    b = _mm_and_si128 (_mm_slli_epi32 (lo, 3), mask_blue);

    rb = _mm_or_si128 (r, b);
    t  = _mm_and_si128 (rb, mask_565_fix_rb);
    t  = _mm_srli_epi32 (t, 5);
    rb = _mm_or_si128 (rb, t);

    t  = _mm_and_si128 (g, mask_565_fix_g);
    t  = _mm_srli_epi32 (t, 6);
    g  = _mm_or_si128 (g, t);

    return _mm_or_si128 (rb, g);
}

static force_inline void
unpack_565_128_4x128 (__m128i  data,
                      __m128i* data0,
                      __m128i* data1,
                      __m128i* data2,
                      __m128i* data3)
{
    __m128i lo, hi;

    lo = _mm_unpacklo_epi16 (data, _mm_setzero_si128 ());
    hi = _mm_unpackhi_epi16 (data, _mm_setzero_si128 ());

    lo = unpack_565_to_8888 (lo);
    hi = unpack_565_to_8888 (hi);

    unpack_128_2x128 (lo, data0, data1);
    unpack_128_2x128 (hi, data2, data3);
}

static force_inline uint16_t
pack_565_32_16 (uint32_t pixel)
{
    return (uint16_t) (((pixel >> 8) & 0xf800) |
		       ((pixel >> 5) & 0x07e0) |
		       ((pixel >> 3) & 0x001f));
}

static force_inline __m128i
pack_2x128_128 (__m128i lo, __m128i hi)
{
    return _mm_packus_epi16 (lo, hi);
}

static force_inline __m128i
pack_565_2x128_128 (__m128i lo, __m128i hi)
{
    __m128i data;
    __m128i r, g1, g2, b;

    data = pack_2x128_128 (lo, hi);

    r  = _mm_and_si128 (data, mask_565_r);
    g1 = _mm_and_si128 (_mm_slli_epi32 (data, 3), mask_565_g1);
    g2 = _mm_and_si128 (_mm_srli_epi32 (data, 5), mask_565_g2);
    b  = _mm_and_si128 (_mm_srli_epi32 (data, 3), mask_565_b);

    return _mm_or_si128 (_mm_or_si128 (_mm_or_si128 (r, g1), g2), b);
}

static force_inline __m128i
pack_565_4x128_128 (__m128i* xmm0, __m128i* xmm1, __m128i* xmm2, __m128i* xmm3)
{
    return _mm_packus_epi16 (pack_565_2x128_128 (*xmm0, *xmm1),
			     pack_565_2x128_128 (*xmm2, *xmm3));
}

static force_inline int
is_opaque (__m128i x)
{
    __m128i ffs = _mm_cmpeq_epi8 (x, x);

    return (_mm_movemask_epi8 (_mm_cmpeq_epi8 (x, ffs)) & 0x8888) == 0x8888;
}

static force_inline int
is_zero (__m128i x)
{
    return _mm_movemask_epi8 (
	_mm_cmpeq_epi8 (x, _mm_setzero_si128 ())) == 0xffff;
}

static force_inline int
is_transparent (__m128i x)
{
    return (_mm_movemask_epi8 (
		_mm_cmpeq_epi8 (x, _mm_setzero_si128 ())) & 0x8888) == 0x8888;
}

static force_inline __m128i
expand_pixel_32_1x128 (uint32_t data)
{
    return _mm_shuffle_epi32 (unpack_32_1x128 (data), _MM_SHUFFLE (1, 0, 1, 0));
}

static force_inline __m128i
expand_alpha_1x128 (__m128i data)
{
    return _mm_shufflehi_epi16 (_mm_shufflelo_epi16 (data,
						     _MM_SHUFFLE (3, 3, 3, 3)),
				_MM_SHUFFLE (3, 3, 3, 3));
}

static force_inline void
expand_alpha_2x128 (__m128i  data_lo,
                    __m128i  data_hi,
                    __m128i* alpha_lo,
                    __m128i* alpha_hi)
{
    __m128i lo, hi;

    lo = _mm_shufflelo_epi16 (data_lo, _MM_SHUFFLE (3, 3, 3, 3));
    hi = _mm_shufflelo_epi16 (data_hi, _MM_SHUFFLE (3, 3, 3, 3));

    *alpha_lo = _mm_shufflehi_epi16 (lo, _MM_SHUFFLE (3, 3, 3, 3));
    *alpha_hi = _mm_shufflehi_epi16 (hi, _MM_SHUFFLE (3, 3, 3, 3));
}

static force_inline void
expand_alpha_rev_2x128 (__m128i  data_lo,
                        __m128i  data_hi,
                        __m128i* alpha_lo,
                        __m128i* alpha_hi)
{
    __m128i lo, hi;

    lo = _mm_shufflelo_epi16 (data_lo, _MM_SHUFFLE (0, 0, 0, 0));
    hi = _mm_shufflelo_epi16 (data_hi, _MM_SHUFFLE (0, 0, 0, 0));
    *alpha_lo = _mm_shufflehi_epi16 (lo, _MM_SHUFFLE (0, 0, 0, 0));
    *alpha_hi = _mm_shufflehi_epi16 (hi, _MM_SHUFFLE (0, 0, 0, 0));
}

static force_inline void
pix_multiply_2x128 (__m128i* data_lo,
                    __m128i* data_hi,
                    __m128i* alpha_lo,
                    __m128i* alpha_hi,
                    __m128i* ret_lo,
                    __m128i* ret_hi)
{
    __m128i lo, hi;

    lo = _mm_mullo_epi16 (*data_lo, *alpha_lo);
    hi = _mm_mullo_epi16 (*data_hi, *alpha_hi);
    lo = _mm_adds_epu16 (lo, mask_0080);
    hi = _mm_adds_epu16 (hi, mask_0080);
    *ret_lo = _mm_mulhi_epu16 (lo, mask_0101);
    *ret_hi = _mm_mulhi_epu16 (hi, mask_0101);
}

static force_inline void
pix_add_multiply_2x128 (__m128i* src_lo,
                        __m128i* src_hi,
                        __m128i* alpha_dst_lo,
                        __m128i* alpha_dst_hi,
                        __m128i* dst_lo,
                        __m128i* dst_hi,
                        __m128i* alpha_src_lo,
                        __m128i* alpha_src_hi,
                        __m128i* ret_lo,
                        __m128i* ret_hi)
{
    __m128i t1_lo, t1_hi;
    __m128i t2_lo, t2_hi;

    pix_multiply_2x128 (src_lo, src_hi, alpha_dst_lo, alpha_dst_hi, &t1_lo, &t1_hi);
    pix_multiply_2x128 (dst_lo, dst_hi, alpha_src_lo, alpha_src_hi, &t2_lo, &t2_hi);

    *ret_lo = _mm_adds_epu8 (t1_lo, t2_lo);
    *ret_hi = _mm_adds_epu8 (t1_hi, t2_hi);
}

static force_inline void
negate_2x128 (__m128i  data_lo,
              __m128i  data_hi,
              __m128i* neg_lo,
              __m128i* neg_hi)
{
    *neg_lo = _mm_xor_si128 (data_lo, mask_00ff);
    *neg_hi = _mm_xor_si128 (data_hi, mask_00ff);
}

static force_inline void
invert_colors_2x128 (__m128i  data_lo,
                     __m128i  data_hi,
                     __m128i* inv_lo,
                     __m128i* inv_hi)
{
    __m128i lo, hi;

    lo = _mm_shufflelo_epi16 (data_lo, _MM_SHUFFLE (3, 0, 1, 2));
    hi = _mm_shufflelo_epi16 (data_hi, _MM_SHUFFLE (3, 0, 1, 2));
    *inv_lo = _mm_shufflehi_epi16 (lo, _MM_SHUFFLE (3, 0, 1, 2));
    *inv_hi = _mm_shufflehi_epi16 (hi, _MM_SHUFFLE (3, 0, 1, 2));
}

static force_inline void
over_2x128 (__m128i* src_lo,
            __m128i* src_hi,
            __m128i* alpha_lo,
            __m128i* alpha_hi,
            __m128i* dst_lo,
            __m128i* dst_hi)
{
    __m128i t1, t2;

    negate_2x128 (*alpha_lo, *alpha_hi, &t1, &t2);

    pix_multiply_2x128 (dst_lo, dst_hi, &t1, &t2, dst_lo, dst_hi);

    *dst_lo = _mm_adds_epu8 (*src_lo, *dst_lo);
    *dst_hi = _mm_adds_epu8 (*src_hi, *dst_hi);
}

static force_inline void
over_rev_non_pre_2x128 (__m128i  src_lo,
                        __m128i  src_hi,
                        __m128i* dst_lo,
                        __m128i* dst_hi)
{
    __m128i lo, hi;
    __m128i alpha_lo, alpha_hi;

    expand_alpha_2x128 (src_lo, src_hi, &alpha_lo, &alpha_hi);

    lo = _mm_or_si128 (alpha_lo, mask_alpha);
    hi = _mm_or_si128 (alpha_hi, mask_alpha);

    invert_colors_2x128 (src_lo, src_hi, &src_lo, &src_hi);

    pix_multiply_2x128 (&src_lo, &src_hi, &lo, &hi, &lo, &hi);

    over_2x128 (&lo, &hi, &alpha_lo, &alpha_hi, dst_lo, dst_hi);
}

static force_inline void
in_over_2x128 (__m128i* src_lo,
               __m128i* src_hi,
               __m128i* alpha_lo,
               __m128i* alpha_hi,
               __m128i* mask_lo,
               __m128i* mask_hi,
               __m128i* dst_lo,
               __m128i* dst_hi)
{
    __m128i s_lo, s_hi;
    __m128i a_lo, a_hi;

    pix_multiply_2x128 (src_lo,   src_hi, mask_lo, mask_hi, &s_lo, &s_hi);
    pix_multiply_2x128 (alpha_lo, alpha_hi, mask_lo, mask_hi, &a_lo, &a_hi);

    over_2x128 (&s_lo, &s_hi, &a_lo, &a_hi, dst_lo, dst_hi);
}

/* load 4 pixels from a 16-byte boundary aligned address */
static force_inline __m128i
load_128_aligned (__m128i* src)
{
    return _mm_load_si128 (src);
}

/* load 4 pixels from a unaligned address */
static force_inline __m128i
load_128_unaligned (const __m128i* src)
{
    return _mm_loadu_si128 (src);
}

/* save 4 pixels using Write Combining memory on a 16-byte
 * boundary aligned address
 */
static force_inline void
save_128_write_combining (__m128i* dst,
                          __m128i  data)
{
    _mm_stream_si128 (dst, data);
}

/* save 4 pixels on a 16-byte boundary aligned address */
static force_inline void
save_128_aligned (__m128i* dst,
                  __m128i  data)
{
    _mm_store_si128 (dst, data);
}

/* save 4 pixels on a unaligned address */
static force_inline void
save_128_unaligned (__m128i* dst,
                    __m128i  data)
{
    _mm_storeu_si128 (dst, data);
}

static force_inline __m128i
load_32_1x128 (uint32_t data)
{
    return _mm_cvtsi32_si128 (data);
}

static force_inline __m128i
expand_alpha_rev_1x128 (__m128i data)
{
    return _mm_shufflelo_epi16 (data, _MM_SHUFFLE (0, 0, 0, 0));
}

static force_inline __m128i
expand_pixel_8_1x128 (uint8_t data)
{
    return _mm_shufflelo_epi16 (
	unpack_32_1x128 ((uint32_t)data), _MM_SHUFFLE (0, 0, 0, 0));
}

static force_inline __m128i
pix_multiply_1x128 (__m128i data,
		    __m128i alpha)
{
    return _mm_mulhi_epu16 (_mm_adds_epu16 (_mm_mullo_epi16 (data, alpha),
					    mask_0080),
			    mask_0101);
}

static force_inline __m128i
pix_add_multiply_1x128 (__m128i* src,
			__m128i* alpha_dst,
			__m128i* dst,
			__m128i* alpha_src)
{
    __m128i t1 = pix_multiply_1x128 (*src, *alpha_dst);
    __m128i t2 = pix_multiply_1x128 (*dst, *alpha_src);

    return _mm_adds_epu8 (t1, t2);
}

static force_inline __m128i
negate_1x128 (__m128i data)
{
    return _mm_xor_si128 (data, mask_00ff);
}

static force_inline __m128i
invert_colors_1x128 (__m128i data)
{
    return _mm_shufflelo_epi16 (data, _MM_SHUFFLE (3, 0, 1, 2));
}

static force_inline __m128i
over_1x128 (__m128i src, __m128i alpha, __m128i dst)
{
    return _mm_adds_epu8 (src, pix_multiply_1x128 (dst, negate_1x128 (alpha)));
}

static force_inline __m128i
in_over_1x128 (__m128i* src, __m128i* alpha, __m128i* mask, __m128i* dst)
{
    return over_1x128 (pix_multiply_1x128 (*src, *mask),
		       pix_multiply_1x128 (*alpha, *mask),
		       *dst);
}

static force_inline __m128i
over_rev_non_pre_1x128 (__m128i src, __m128i dst)
{
    __m128i alpha = expand_alpha_1x128 (src);

    return over_1x128 (pix_multiply_1x128 (invert_colors_1x128 (src),
					   _mm_or_si128 (alpha, mask_alpha)),
		       alpha,
		       dst);
}

static force_inline uint32_t
pack_1x128_32 (__m128i data)
{
    return _mm_cvtsi128_si32 (_mm_packus_epi16 (data, _mm_setzero_si128 ()));
}

static force_inline __m128i
expand565_16_1x128 (uint16_t pixel)
{
    __m128i m = _mm_cvtsi32_si128 (pixel);

    m = unpack_565_to_8888 (m);

    return _mm_unpacklo_epi8 (m, _mm_setzero_si128 ());
}

static force_inline uint32_t
core_combine_over_u_pixel_sse2 (uint32_t src, uint32_t dst)
{
    uint8_t a;
    __m128i xmms;

    a = src >> 24;

    if (a == 0xff)
    {
	return src;
    }
    else if (src)
    {
	xmms = unpack_32_1x128 (src);
	return pack_1x128_32 (
	    over_1x128 (xmms, expand_alpha_1x128 (xmms),
			unpack_32_1x128 (dst)));
    }

    return dst;
}

static force_inline uint32_t
combine1 (const uint32_t *ps, const uint32_t *pm)
{
    uint32_t s = *ps;

    if (pm)
    {
	__m128i ms, mm;

	mm = unpack_32_1x128 (*pm);
	mm = expand_alpha_1x128 (mm);

	ms = unpack_32_1x128 (s);
	ms = pix_multiply_1x128 (ms, mm);

	s = pack_1x128_32 (ms);
    }

    return s;
}

static force_inline __m128i
combine4 (const __m128i *ps, const __m128i *pm)
{
    __m128i xmm_src_lo, xmm_src_hi;
    __m128i xmm_msk_lo, xmm_msk_hi;
    __m128i s;

    if (pm)
    {
	xmm_msk_lo = load_128_unaligned (pm);

	if (is_transparent (xmm_msk_lo))
	    return _mm_setzero_si128 ();
    }

    s = load_128_unaligned (ps);

    if (pm)
    {
	unpack_128_2x128 (s, &xmm_src_lo, &xmm_src_hi);
	unpack_128_2x128 (xmm_msk_lo, &xmm_msk_lo, &xmm_msk_hi);

	expand_alpha_2x128 (xmm_msk_lo, xmm_msk_hi, &xmm_msk_lo, &xmm_msk_hi);

	pix_multiply_2x128 (&xmm_src_lo, &xmm_src_hi,
			    &xmm_msk_lo, &xmm_msk_hi,
			    &xmm_src_lo, &xmm_src_hi);

	s = pack_2x128_128 (xmm_src_lo, xmm_src_hi);
    }

    return s;
}

static force_inline void
core_combine_over_u_sse2_mask (uint32_t *	  pd,
			       const uint32_t*    ps,
			       const uint32_t*    pm,
			       int                w)
{
    uint32_t s, d;

    /* Align dst on a 16-byte boundary */
    while (w && ((unsigned long)pd & 15))
    {
	d = *pd;
	s = combine1 (ps, pm);

	if (s)
	    *pd = core_combine_over_u_pixel_sse2 (s, d);
	pd++;
	ps++;
	pm++;
	w--;
    }

    while (w >= 4)
    {
	__m128i mask = load_128_unaligned ((__m128i *)pm);

	if (!is_zero (mask))
	{
	    __m128i src;
	    __m128i src_hi, src_lo;
	    __m128i mask_hi, mask_lo;
	    __m128i alpha_hi, alpha_lo;

	    src = load_128_unaligned ((__m128i *)ps);

	    if (is_opaque (_mm_and_si128 (src, mask)))
	    {
		save_128_aligned ((__m128i *)pd, src);
	    }
	    else
	    {
		__m128i dst = load_128_aligned ((__m128i *)pd);
		__m128i dst_hi, dst_lo;

		unpack_128_2x128 (mask, &mask_lo, &mask_hi);
		unpack_128_2x128 (src, &src_lo, &src_hi);

		expand_alpha_2x128 (mask_lo, mask_hi, &mask_lo, &mask_hi);
		pix_multiply_2x128 (&src_lo, &src_hi,
				    &mask_lo, &mask_hi,
				    &src_lo, &src_hi);

		unpack_128_2x128 (dst, &dst_lo, &dst_hi);

		expand_alpha_2x128 (src_lo, src_hi,
				    &alpha_lo, &alpha_hi);

		over_2x128 (&src_lo, &src_hi, &alpha_lo, &alpha_hi,
			    &dst_lo, &dst_hi);

		save_128_aligned (
		    (__m128i *)pd,
		    pack_2x128_128 (dst_lo, dst_hi));
	    }
	}

	pm += 4;
	ps += 4;
	pd += 4;
	w -= 4;
    }
    while (w)
    {
	d = *pd;
	s = combine1 (ps, pm);

	if (s)
	    *pd = core_combine_over_u_pixel_sse2 (s, d);
	pd++;
	ps++;
	pm++;

	w--;
    }
}

static force_inline void
core_combine_over_u_sse2_no_mask (uint32_t *	  pd,
				  const uint32_t*    ps,
				  int                w)
{
    uint32_t s, d;

    /* Align dst on a 16-byte boundary */
    while (w && ((unsigned long)pd & 15))
    {
	d = *pd;
	s = *ps;

	if (s)
	    *pd = core_combine_over_u_pixel_sse2 (s, d);
	pd++;
	ps++;
	w--;
    }

    while (w >= 4)
    {
	__m128i src;
	__m128i src_hi, src_lo, dst_hi, dst_lo;
	__m128i alpha_hi, alpha_lo;

	src = load_128_unaligned ((__m128i *)ps);

	if (!is_zero (src))
	{
	    if (is_opaque (src))
	    {
		save_128_aligned ((__m128i *)pd, src);
	    }
	    else
	    {
		__m128i dst = load_128_aligned ((__m128i *)pd);

		unpack_128_2x128 (src, &src_lo, &src_hi);
		unpack_128_2x128 (dst, &dst_lo, &dst_hi);

		expand_alpha_2x128 (src_lo, src_hi,
				    &alpha_lo, &alpha_hi);
		over_2x128 (&src_lo, &src_hi, &alpha_lo, &alpha_hi,
			    &dst_lo, &dst_hi);

		save_128_aligned (
		    (__m128i *)pd,
		    pack_2x128_128 (dst_lo, dst_hi));
	    }
	}

	ps += 4;
	pd += 4;
	w -= 4;
    }
    while (w)
    {
	d = *pd;
	s = *ps;

	if (s)
	    *pd = core_combine_over_u_pixel_sse2 (s, d);
	pd++;
	ps++;

	w--;
    }
}

static force_inline void
sse2_combine_over_u (pixman_implementation_t *imp,
                     pixman_op_t              op,
                     uint32_t *               pd,
                     const uint32_t *         ps,
                     const uint32_t *         pm,
                     int                      w)
{
    if (pm)
	core_combine_over_u_sse2_mask (pd, ps, pm, w);
    else
	core_combine_over_u_sse2_no_mask (pd, ps, w);
}

static void
sse2_combine_over_reverse_u (pixman_implementation_t *imp,
                             pixman_op_t              op,
                             uint32_t *               pd,
                             const uint32_t *         ps,
                             const uint32_t *         pm,
                             int                      w)
{
    uint32_t s, d;

    __m128i xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_src_lo, xmm_src_hi;
    __m128i xmm_alpha_lo, xmm_alpha_hi;

    /* Align dst on a 16-byte boundary */
    while (w &&
           ((unsigned long)pd & 15))
    {
	d = *pd;
	s = combine1 (ps, pm);

	*pd++ = core_combine_over_u_pixel_sse2 (d, s);
	w--;
	ps++;
	if (pm)
	    pm++;
    }

    while (w >= 4)
    {
	/* I'm loading unaligned because I'm not sure
	 * about the address alignment.
	 */
	xmm_src_hi = combine4 ((__m128i*)ps, (__m128i*)pm);
	xmm_dst_hi = load_128_aligned ((__m128i*) pd);

	unpack_128_2x128 (xmm_src_hi, &xmm_src_lo, &xmm_src_hi);
	unpack_128_2x128 (xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);

	expand_alpha_2x128 (xmm_dst_lo, xmm_dst_hi,
			    &xmm_alpha_lo, &xmm_alpha_hi);

	over_2x128 (&xmm_dst_lo, &xmm_dst_hi,
		    &xmm_alpha_lo, &xmm_alpha_hi,
		    &xmm_src_lo, &xmm_src_hi);

	/* rebuid the 4 pixel data and save*/
	save_128_aligned ((__m128i*)pd,
			  pack_2x128_128 (xmm_src_lo, xmm_src_hi));

	w -= 4;
	ps += 4;
	pd += 4;

	if (pm)
	    pm += 4;
    }

    while (w)
    {
	d = *pd;
	s = combine1 (ps, pm);

	*pd++ = core_combine_over_u_pixel_sse2 (d, s);
	ps++;
	w--;
	if (pm)
	    pm++;
    }
}

static force_inline uint32_t
core_combine_in_u_pixel_sse2 (uint32_t src, uint32_t dst)
{
    uint32_t maska = src >> 24;

    if (maska == 0)
    {
	return 0;
    }
    else if (maska != 0xff)
    {
	return pack_1x128_32 (
	    pix_multiply_1x128 (unpack_32_1x128 (dst),
				expand_alpha_1x128 (unpack_32_1x128 (src))));
    }

    return dst;
}

static void
sse2_combine_in_u (pixman_implementation_t *imp,
                   pixman_op_t              op,
                   uint32_t *               pd,
                   const uint32_t *         ps,
                   const uint32_t *         pm,
                   int                      w)
{
    uint32_t s, d;

    __m128i xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst_lo, xmm_dst_hi;

    while (w && ((unsigned long) pd & 15))
    {
	s = combine1 (ps, pm);
	d = *pd;

	*pd++ = core_combine_in_u_pixel_sse2 (d, s);
	w--;
	ps++;
	if (pm)
	    pm++;
    }

    while (w >= 4)
    {
	xmm_dst_hi = load_128_aligned ((__m128i*) pd);
	xmm_src_hi = combine4 ((__m128i*) ps, (__m128i*) pm);

	unpack_128_2x128 (xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);
	expand_alpha_2x128 (xmm_dst_lo, xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);

	unpack_128_2x128 (xmm_src_hi, &xmm_src_lo, &xmm_src_hi);
	pix_multiply_2x128 (&xmm_src_lo, &xmm_src_hi,
			    &xmm_dst_lo, &xmm_dst_hi,
			    &xmm_dst_lo, &xmm_dst_hi);

	save_128_aligned ((__m128i*)pd,
			  pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	ps += 4;
	pd += 4;
	w -= 4;
	if (pm)
	    pm += 4;
    }

    while (w)
    {
	s = combine1 (ps, pm);
	d = *pd;

	*pd++ = core_combine_in_u_pixel_sse2 (d, s);
	w--;
	ps++;
	if (pm)
	    pm++;
    }
}

static void
sse2_combine_in_reverse_u (pixman_implementation_t *imp,
                           pixman_op_t              op,
                           uint32_t *               pd,
                           const uint32_t *         ps,
                           const uint32_t *         pm,
                           int                      w)
{
    uint32_t s, d;

    __m128i xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst_lo, xmm_dst_hi;

    while (w && ((unsigned long) pd & 15))
    {
	s = combine1 (ps, pm);
	d = *pd;

	*pd++ = core_combine_in_u_pixel_sse2 (s, d);
	ps++;
	w--;
	if (pm)
	    pm++;
    }

    while (w >= 4)
    {
	xmm_dst_hi = load_128_aligned ((__m128i*) pd);
	xmm_src_hi = combine4 ((__m128i*) ps, (__m128i*)pm);

	unpack_128_2x128 (xmm_src_hi, &xmm_src_lo, &xmm_src_hi);
	expand_alpha_2x128 (xmm_src_lo, xmm_src_hi, &xmm_src_lo, &xmm_src_hi);

	unpack_128_2x128 (xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);
	pix_multiply_2x128 (&xmm_dst_lo, &xmm_dst_hi,
			    &xmm_src_lo, &xmm_src_hi,
			    &xmm_dst_lo, &xmm_dst_hi);

	save_128_aligned (
	    (__m128i*)pd, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	ps += 4;
	pd += 4;
	w -= 4;
	if (pm)
	    pm += 4;
    }

    while (w)
    {
	s = combine1 (ps, pm);
	d = *pd;

	*pd++ = core_combine_in_u_pixel_sse2 (s, d);
	w--;
	ps++;
	if (pm)
	    pm++;
    }
}

static void
sse2_combine_out_reverse_u (pixman_implementation_t *imp,
                            pixman_op_t              op,
                            uint32_t *               pd,
                            const uint32_t *         ps,
                            const uint32_t *         pm,
                            int                      w)
{
    while (w && ((unsigned long) pd & 15))
    {
	uint32_t s = combine1 (ps, pm);
	uint32_t d = *pd;

	*pd++ = pack_1x128_32 (
	    pix_multiply_1x128 (
		unpack_32_1x128 (d), negate_1x128 (
		    expand_alpha_1x128 (unpack_32_1x128 (s)))));

	if (pm)
	    pm++;
	ps++;
	w--;
    }

    while (w >= 4)
    {
	__m128i xmm_src_lo, xmm_src_hi;
	__m128i xmm_dst_lo, xmm_dst_hi;

	xmm_src_hi = combine4 ((__m128i*)ps, (__m128i*)pm);
	xmm_dst_hi = load_128_aligned ((__m128i*) pd);

	unpack_128_2x128 (xmm_src_hi, &xmm_src_lo, &xmm_src_hi);
	unpack_128_2x128 (xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);

	expand_alpha_2x128 (xmm_src_lo, xmm_src_hi, &xmm_src_lo, &xmm_src_hi);
	negate_2x128       (xmm_src_lo, xmm_src_hi, &xmm_src_lo, &xmm_src_hi);

	pix_multiply_2x128 (&xmm_dst_lo, &xmm_dst_hi,
			    &xmm_src_lo, &xmm_src_hi,
			    &xmm_dst_lo, &xmm_dst_hi);

	save_128_aligned (
	    (__m128i*)pd, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	ps += 4;
	pd += 4;
	if (pm)
	    pm += 4;

	w -= 4;
    }

    while (w)
    {
	uint32_t s = combine1 (ps, pm);
	uint32_t d = *pd;

	*pd++ = pack_1x128_32 (
	    pix_multiply_1x128 (
		unpack_32_1x128 (d), negate_1x128 (
		    expand_alpha_1x128 (unpack_32_1x128 (s)))));
	ps++;
	if (pm)
	    pm++;
	w--;
    }
}

static void
sse2_combine_out_u (pixman_implementation_t *imp,
                    pixman_op_t              op,
                    uint32_t *               pd,
                    const uint32_t *         ps,
                    const uint32_t *         pm,
                    int                      w)
{
    while (w && ((unsigned long) pd & 15))
    {
	uint32_t s = combine1 (ps, pm);
	uint32_t d = *pd;

	*pd++ = pack_1x128_32 (
	    pix_multiply_1x128 (
		unpack_32_1x128 (s), negate_1x128 (
		    expand_alpha_1x128 (unpack_32_1x128 (d)))));
	w--;
	ps++;
	if (pm)
	    pm++;
    }

    while (w >= 4)
    {
	__m128i xmm_src_lo, xmm_src_hi;
	__m128i xmm_dst_lo, xmm_dst_hi;

	xmm_src_hi = combine4 ((__m128i*) ps, (__m128i*)pm);
	xmm_dst_hi = load_128_aligned ((__m128i*) pd);

	unpack_128_2x128 (xmm_src_hi, &xmm_src_lo, &xmm_src_hi);
	unpack_128_2x128 (xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);

	expand_alpha_2x128 (xmm_dst_lo, xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);
	negate_2x128       (xmm_dst_lo, xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);

	pix_multiply_2x128 (&xmm_src_lo, &xmm_src_hi,
			    &xmm_dst_lo, &xmm_dst_hi,
			    &xmm_dst_lo, &xmm_dst_hi);

	save_128_aligned (
	    (__m128i*)pd, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	ps += 4;
	pd += 4;
	w -= 4;
	if (pm)
	    pm += 4;
    }

    while (w)
    {
	uint32_t s = combine1 (ps, pm);
	uint32_t d = *pd;

	*pd++ = pack_1x128_32 (
	    pix_multiply_1x128 (
		unpack_32_1x128 (s), negate_1x128 (
		    expand_alpha_1x128 (unpack_32_1x128 (d)))));
	w--;
	ps++;
	if (pm)
	    pm++;
    }
}

static force_inline uint32_t
core_combine_atop_u_pixel_sse2 (uint32_t src,
                                uint32_t dst)
{
    __m128i s = unpack_32_1x128 (src);
    __m128i d = unpack_32_1x128 (dst);

    __m128i sa = negate_1x128 (expand_alpha_1x128 (s));
    __m128i da = expand_alpha_1x128 (d);

    return pack_1x128_32 (pix_add_multiply_1x128 (&s, &da, &d, &sa));
}

static void
sse2_combine_atop_u (pixman_implementation_t *imp,
                     pixman_op_t              op,
                     uint32_t *               pd,
                     const uint32_t *         ps,
                     const uint32_t *         pm,
                     int                      w)
{
    uint32_t s, d;

    __m128i xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_alpha_src_lo, xmm_alpha_src_hi;
    __m128i xmm_alpha_dst_lo, xmm_alpha_dst_hi;

    while (w && ((unsigned long) pd & 15))
    {
	s = combine1 (ps, pm);
	d = *pd;

	*pd++ = core_combine_atop_u_pixel_sse2 (s, d);
	w--;
	ps++;
	if (pm)
	    pm++;
    }

    while (w >= 4)
    {
	xmm_src_hi = combine4 ((__m128i*)ps, (__m128i*)pm);
	xmm_dst_hi = load_128_aligned ((__m128i*) pd);

	unpack_128_2x128 (xmm_src_hi, &xmm_src_lo, &xmm_src_hi);
	unpack_128_2x128 (xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);

	expand_alpha_2x128 (xmm_src_lo, xmm_src_hi,
			    &xmm_alpha_src_lo, &xmm_alpha_src_hi);
	expand_alpha_2x128 (xmm_dst_lo, xmm_dst_hi,
			    &xmm_alpha_dst_lo, &xmm_alpha_dst_hi);

	negate_2x128 (xmm_alpha_src_lo, xmm_alpha_src_hi,
		      &xmm_alpha_src_lo, &xmm_alpha_src_hi);

	pix_add_multiply_2x128 (
	    &xmm_src_lo, &xmm_src_hi, &xmm_alpha_dst_lo, &xmm_alpha_dst_hi,
	    &xmm_dst_lo, &xmm_dst_hi, &xmm_alpha_src_lo, &xmm_alpha_src_hi,
	    &xmm_dst_lo, &xmm_dst_hi);

	save_128_aligned (
	    (__m128i*)pd, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	ps += 4;
	pd += 4;
	w -= 4;
	if (pm)
	    pm += 4;
    }

    while (w)
    {
	s = combine1 (ps, pm);
	d = *pd;

	*pd++ = core_combine_atop_u_pixel_sse2 (s, d);
	w--;
	ps++;
	if (pm)
	    pm++;
    }
}

static force_inline uint32_t
core_combine_reverse_atop_u_pixel_sse2 (uint32_t src,
                                        uint32_t dst)
{
    __m128i s = unpack_32_1x128 (src);
    __m128i d = unpack_32_1x128 (dst);

    __m128i sa = expand_alpha_1x128 (s);
    __m128i da = negate_1x128 (expand_alpha_1x128 (d));

    return pack_1x128_32 (pix_add_multiply_1x128 (&s, &da, &d, &sa));
}

static void
sse2_combine_atop_reverse_u (pixman_implementation_t *imp,
                             pixman_op_t              op,
                             uint32_t *               pd,
                             const uint32_t *         ps,
                             const uint32_t *         pm,
                             int                      w)
{
    uint32_t s, d;

    __m128i xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_alpha_src_lo, xmm_alpha_src_hi;
    __m128i xmm_alpha_dst_lo, xmm_alpha_dst_hi;

    while (w && ((unsigned long) pd & 15))
    {
	s = combine1 (ps, pm);
	d = *pd;

	*pd++ = core_combine_reverse_atop_u_pixel_sse2 (s, d);
	ps++;
	w--;
	if (pm)
	    pm++;
    }

    while (w >= 4)
    {
	xmm_src_hi = combine4 ((__m128i*)ps, (__m128i*)pm);
	xmm_dst_hi = load_128_aligned ((__m128i*) pd);

	unpack_128_2x128 (xmm_src_hi, &xmm_src_lo, &xmm_src_hi);
	unpack_128_2x128 (xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);

	expand_alpha_2x128 (xmm_src_lo, xmm_src_hi,
			    &xmm_alpha_src_lo, &xmm_alpha_src_hi);
	expand_alpha_2x128 (xmm_dst_lo, xmm_dst_hi,
			    &xmm_alpha_dst_lo, &xmm_alpha_dst_hi);

	negate_2x128 (xmm_alpha_dst_lo, xmm_alpha_dst_hi,
		      &xmm_alpha_dst_lo, &xmm_alpha_dst_hi);

	pix_add_multiply_2x128 (
	    &xmm_src_lo, &xmm_src_hi, &xmm_alpha_dst_lo, &xmm_alpha_dst_hi,
	    &xmm_dst_lo, &xmm_dst_hi, &xmm_alpha_src_lo, &xmm_alpha_src_hi,
	    &xmm_dst_lo, &xmm_dst_hi);

	save_128_aligned (
	    (__m128i*)pd, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	ps += 4;
	pd += 4;
	w -= 4;
	if (pm)
	    pm += 4;
    }

    while (w)
    {
	s = combine1 (ps, pm);
	d = *pd;

	*pd++ = core_combine_reverse_atop_u_pixel_sse2 (s, d);
	ps++;
	w--;
	if (pm)
	    pm++;
    }
}

static force_inline uint32_t
core_combine_xor_u_pixel_sse2 (uint32_t src,
                               uint32_t dst)
{
    __m128i s = unpack_32_1x128 (src);
    __m128i d = unpack_32_1x128 (dst);

    __m128i neg_d = negate_1x128 (expand_alpha_1x128 (d));
    __m128i neg_s = negate_1x128 (expand_alpha_1x128 (s));

    return pack_1x128_32 (pix_add_multiply_1x128 (&s, &neg_d, &d, &neg_s));
}

static void
sse2_combine_xor_u (pixman_implementation_t *imp,
                    pixman_op_t              op,
                    uint32_t *               dst,
                    const uint32_t *         src,
                    const uint32_t *         mask,
                    int                      width)
{
    int w = width;
    uint32_t s, d;
    uint32_t* pd = dst;
    const uint32_t* ps = src;
    const uint32_t* pm = mask;

    __m128i xmm_src, xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst, xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_alpha_src_lo, xmm_alpha_src_hi;
    __m128i xmm_alpha_dst_lo, xmm_alpha_dst_hi;

    while (w && ((unsigned long) pd & 15))
    {
	s = combine1 (ps, pm);
	d = *pd;

	*pd++ = core_combine_xor_u_pixel_sse2 (s, d);
	w--;
	ps++;
	if (pm)
	    pm++;
    }

    while (w >= 4)
    {
	xmm_src = combine4 ((__m128i*) ps, (__m128i*) pm);
	xmm_dst = load_128_aligned ((__m128i*) pd);

	unpack_128_2x128 (xmm_src, &xmm_src_lo, &xmm_src_hi);
	unpack_128_2x128 (xmm_dst, &xmm_dst_lo, &xmm_dst_hi);

	expand_alpha_2x128 (xmm_src_lo, xmm_src_hi,
			    &xmm_alpha_src_lo, &xmm_alpha_src_hi);
	expand_alpha_2x128 (xmm_dst_lo, xmm_dst_hi,
			    &xmm_alpha_dst_lo, &xmm_alpha_dst_hi);

	negate_2x128 (xmm_alpha_src_lo, xmm_alpha_src_hi,
		      &xmm_alpha_src_lo, &xmm_alpha_src_hi);
	negate_2x128 (xmm_alpha_dst_lo, xmm_alpha_dst_hi,
		      &xmm_alpha_dst_lo, &xmm_alpha_dst_hi);

	pix_add_multiply_2x128 (
	    &xmm_src_lo, &xmm_src_hi, &xmm_alpha_dst_lo, &xmm_alpha_dst_hi,
	    &xmm_dst_lo, &xmm_dst_hi, &xmm_alpha_src_lo, &xmm_alpha_src_hi,
	    &xmm_dst_lo, &xmm_dst_hi);

	save_128_aligned (
	    (__m128i*)pd, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	ps += 4;
	pd += 4;
	w -= 4;
	if (pm)
	    pm += 4;
    }

    while (w)
    {
	s = combine1 (ps, pm);
	d = *pd;

	*pd++ = core_combine_xor_u_pixel_sse2 (s, d);
	w--;
	ps++;
	if (pm)
	    pm++;
    }
}

static force_inline void
sse2_combine_add_u (pixman_implementation_t *imp,
                    pixman_op_t              op,
                    uint32_t *               dst,
                    const uint32_t *         src,
                    const uint32_t *         mask,
                    int                      width)
{
    int w = width;
    uint32_t s, d;
    uint32_t* pd = dst;
    const uint32_t* ps = src;
    const uint32_t* pm = mask;

    while (w && (unsigned long)pd & 15)
    {
	s = combine1 (ps, pm);
	d = *pd;

	ps++;
	if (pm)
	    pm++;
	*pd++ = _mm_cvtsi128_si32 (
	    _mm_adds_epu8 (_mm_cvtsi32_si128 (s), _mm_cvtsi32_si128 (d)));
	w--;
    }

    while (w >= 4)
    {
	__m128i s;

	s = combine4 ((__m128i*)ps, (__m128i*)pm);

	save_128_aligned (
	    (__m128i*)pd, _mm_adds_epu8 (s, load_128_aligned  ((__m128i*)pd)));

	pd += 4;
	ps += 4;
	if (pm)
	    pm += 4;
	w -= 4;
    }

    while (w--)
    {
	s = combine1 (ps, pm);
	d = *pd;

	ps++;
	*pd++ = _mm_cvtsi128_si32 (
	    _mm_adds_epu8 (_mm_cvtsi32_si128 (s), _mm_cvtsi32_si128 (d)));
	if (pm)
	    pm++;
    }
}

static force_inline uint32_t
core_combine_saturate_u_pixel_sse2 (uint32_t src,
                                    uint32_t dst)
{
    __m128i ms = unpack_32_1x128 (src);
    __m128i md = unpack_32_1x128 (dst);
    uint32_t sa = src >> 24;
    uint32_t da = ~dst >> 24;

    if (sa > da)
    {
	ms = pix_multiply_1x128 (
	    ms, expand_alpha_1x128 (unpack_32_1x128 (DIV_UN8 (da, sa) << 24)));
    }

    return pack_1x128_32 (_mm_adds_epu16 (md, ms));
}

static void
sse2_combine_saturate_u (pixman_implementation_t *imp,
                         pixman_op_t              op,
                         uint32_t *               pd,
                         const uint32_t *         ps,
                         const uint32_t *         pm,
                         int                      w)
{
    uint32_t s, d;

    uint32_t pack_cmp;
    __m128i xmm_src, xmm_dst;

    while (w && (unsigned long)pd & 15)
    {
	s = combine1 (ps, pm);
	d = *pd;

	*pd++ = core_combine_saturate_u_pixel_sse2 (s, d);
	w--;
	ps++;
	if (pm)
	    pm++;
    }

    while (w >= 4)
    {
	xmm_dst = load_128_aligned  ((__m128i*)pd);
	xmm_src = combine4 ((__m128i*)ps, (__m128i*)pm);

	pack_cmp = _mm_movemask_epi8 (
	    _mm_cmpgt_epi32 (
		_mm_srli_epi32 (xmm_src, 24),
		_mm_srli_epi32 (_mm_xor_si128 (xmm_dst, mask_ff000000), 24)));

	/* if some alpha src is grater than respective ~alpha dst */
	if (pack_cmp)
	{
	    s = combine1 (ps++, pm);
	    d = *pd;
	    *pd++ = core_combine_saturate_u_pixel_sse2 (s, d);
	    if (pm)
		pm++;

	    s = combine1 (ps++, pm);
	    d = *pd;
	    *pd++ = core_combine_saturate_u_pixel_sse2 (s, d);
	    if (pm)
		pm++;

	    s = combine1 (ps++, pm);
	    d = *pd;
	    *pd++ = core_combine_saturate_u_pixel_sse2 (s, d);
	    if (pm)
		pm++;

	    s = combine1 (ps++, pm);
	    d = *pd;
	    *pd++ = core_combine_saturate_u_pixel_sse2 (s, d);
	    if (pm)
		pm++;
	}
	else
	{
	    save_128_aligned ((__m128i*)pd, _mm_adds_epu8 (xmm_dst, xmm_src));

	    pd += 4;
	    ps += 4;
	    if (pm)
		pm += 4;
	}

	w -= 4;
    }

    while (w--)
    {
	s = combine1 (ps, pm);
	d = *pd;

	*pd++ = core_combine_saturate_u_pixel_sse2 (s, d);
	ps++;
	if (pm)
	    pm++;
    }
}

static void
sse2_combine_src_ca (pixman_implementation_t *imp,
                     pixman_op_t              op,
                     uint32_t *               pd,
                     const uint32_t *         ps,
                     const uint32_t *         pm,
                     int                      w)
{
    uint32_t s, m;

    __m128i xmm_src_lo, xmm_src_hi;
    __m128i xmm_mask_lo, xmm_mask_hi;
    __m128i xmm_dst_lo, xmm_dst_hi;

    while (w && (unsigned long)pd & 15)
    {
	s = *ps++;
	m = *pm++;
	*pd++ = pack_1x128_32 (
	    pix_multiply_1x128 (unpack_32_1x128 (s), unpack_32_1x128 (m)));
	w--;
    }

    while (w >= 4)
    {
	xmm_src_hi = load_128_unaligned ((__m128i*)ps);
	xmm_mask_hi = load_128_unaligned ((__m128i*)pm);

	unpack_128_2x128 (xmm_src_hi, &xmm_src_lo, &xmm_src_hi);
	unpack_128_2x128 (xmm_mask_hi, &xmm_mask_lo, &xmm_mask_hi);

	pix_multiply_2x128 (&xmm_src_lo, &xmm_src_hi,
			    &xmm_mask_lo, &xmm_mask_hi,
			    &xmm_dst_lo, &xmm_dst_hi);

	save_128_aligned (
	    (__m128i*)pd, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	ps += 4;
	pd += 4;
	pm += 4;
	w -= 4;
    }

    while (w)
    {
	s = *ps++;
	m = *pm++;
	*pd++ = pack_1x128_32 (
	    pix_multiply_1x128 (unpack_32_1x128 (s), unpack_32_1x128 (m)));
	w--;
    }
}

static force_inline uint32_t
core_combine_over_ca_pixel_sse2 (uint32_t src,
                                 uint32_t mask,
                                 uint32_t dst)
{
    __m128i s = unpack_32_1x128 (src);
    __m128i expAlpha = expand_alpha_1x128 (s);
    __m128i unpk_mask = unpack_32_1x128 (mask);
    __m128i unpk_dst  = unpack_32_1x128 (dst);

    return pack_1x128_32 (in_over_1x128 (&s, &expAlpha, &unpk_mask, &unpk_dst));
}

static void
sse2_combine_over_ca (pixman_implementation_t *imp,
                      pixman_op_t              op,
                      uint32_t *               pd,
                      const uint32_t *         ps,
                      const uint32_t *         pm,
                      int                      w)
{
    uint32_t s, m, d;

    __m128i xmm_alpha_lo, xmm_alpha_hi;
    __m128i xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_mask_lo, xmm_mask_hi;

    while (w && (unsigned long)pd & 15)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = core_combine_over_ca_pixel_sse2 (s, m, d);
	w--;
    }

    while (w >= 4)
    {
	xmm_dst_hi = load_128_aligned ((__m128i*)pd);
	xmm_src_hi = load_128_unaligned ((__m128i*)ps);
	xmm_mask_hi = load_128_unaligned ((__m128i*)pm);

	unpack_128_2x128 (xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);
	unpack_128_2x128 (xmm_src_hi, &xmm_src_lo, &xmm_src_hi);
	unpack_128_2x128 (xmm_mask_hi, &xmm_mask_lo, &xmm_mask_hi);

	expand_alpha_2x128 (xmm_src_lo, xmm_src_hi,
			    &xmm_alpha_lo, &xmm_alpha_hi);

	in_over_2x128 (&xmm_src_lo, &xmm_src_hi,
		       &xmm_alpha_lo, &xmm_alpha_hi,
		       &xmm_mask_lo, &xmm_mask_hi,
		       &xmm_dst_lo, &xmm_dst_hi);

	save_128_aligned (
	    (__m128i*)pd, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	ps += 4;
	pd += 4;
	pm += 4;
	w -= 4;
    }

    while (w)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = core_combine_over_ca_pixel_sse2 (s, m, d);
	w--;
    }
}

static force_inline uint32_t
core_combine_over_reverse_ca_pixel_sse2 (uint32_t src,
                                         uint32_t mask,
                                         uint32_t dst)
{
    __m128i d = unpack_32_1x128 (dst);

    return pack_1x128_32 (
	over_1x128 (d, expand_alpha_1x128 (d),
		    pix_multiply_1x128 (unpack_32_1x128 (src),
					unpack_32_1x128 (mask))));
}

static void
sse2_combine_over_reverse_ca (pixman_implementation_t *imp,
                              pixman_op_t              op,
                              uint32_t *               pd,
                              const uint32_t *         ps,
                              const uint32_t *         pm,
                              int                      w)
{
    uint32_t s, m, d;

    __m128i xmm_alpha_lo, xmm_alpha_hi;
    __m128i xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_mask_lo, xmm_mask_hi;

    while (w && (unsigned long)pd & 15)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = core_combine_over_reverse_ca_pixel_sse2 (s, m, d);
	w--;
    }

    while (w >= 4)
    {
	xmm_dst_hi = load_128_aligned ((__m128i*)pd);
	xmm_src_hi = load_128_unaligned ((__m128i*)ps);
	xmm_mask_hi = load_128_unaligned ((__m128i*)pm);

	unpack_128_2x128 (xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);
	unpack_128_2x128 (xmm_src_hi, &xmm_src_lo, &xmm_src_hi);
	unpack_128_2x128 (xmm_mask_hi, &xmm_mask_lo, &xmm_mask_hi);

	expand_alpha_2x128 (xmm_dst_lo, xmm_dst_hi,
			    &xmm_alpha_lo, &xmm_alpha_hi);
	pix_multiply_2x128 (&xmm_src_lo, &xmm_src_hi,
			    &xmm_mask_lo, &xmm_mask_hi,
			    &xmm_mask_lo, &xmm_mask_hi);

	over_2x128 (&xmm_dst_lo, &xmm_dst_hi,
		    &xmm_alpha_lo, &xmm_alpha_hi,
		    &xmm_mask_lo, &xmm_mask_hi);

	save_128_aligned (
	    (__m128i*)pd, pack_2x128_128 (xmm_mask_lo, xmm_mask_hi));

	ps += 4;
	pd += 4;
	pm += 4;
	w -= 4;
    }

    while (w)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = core_combine_over_reverse_ca_pixel_sse2 (s, m, d);
	w--;
    }
}

static void
sse2_combine_in_ca (pixman_implementation_t *imp,
                    pixman_op_t              op,
                    uint32_t *               pd,
                    const uint32_t *         ps,
                    const uint32_t *         pm,
                    int                      w)
{
    uint32_t s, m, d;

    __m128i xmm_alpha_lo, xmm_alpha_hi;
    __m128i xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_mask_lo, xmm_mask_hi;

    while (w && (unsigned long)pd & 15)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = pack_1x128_32 (
	    pix_multiply_1x128 (
		pix_multiply_1x128 (unpack_32_1x128 (s), unpack_32_1x128 (m)),
		expand_alpha_1x128 (unpack_32_1x128 (d))));

	w--;
    }

    while (w >= 4)
    {
	xmm_dst_hi = load_128_aligned ((__m128i*)pd);
	xmm_src_hi = load_128_unaligned ((__m128i*)ps);
	xmm_mask_hi = load_128_unaligned ((__m128i*)pm);

	unpack_128_2x128 (xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);
	unpack_128_2x128 (xmm_src_hi, &xmm_src_lo, &xmm_src_hi);
	unpack_128_2x128 (xmm_mask_hi, &xmm_mask_lo, &xmm_mask_hi);

	expand_alpha_2x128 (xmm_dst_lo, xmm_dst_hi,
			    &xmm_alpha_lo, &xmm_alpha_hi);

	pix_multiply_2x128 (&xmm_src_lo, &xmm_src_hi,
			    &xmm_mask_lo, &xmm_mask_hi,
			    &xmm_dst_lo, &xmm_dst_hi);

	pix_multiply_2x128 (&xmm_dst_lo, &xmm_dst_hi,
			    &xmm_alpha_lo, &xmm_alpha_hi,
			    &xmm_dst_lo, &xmm_dst_hi);

	save_128_aligned (
	    (__m128i*)pd, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	ps += 4;
	pd += 4;
	pm += 4;
	w -= 4;
    }

    while (w)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = pack_1x128_32 (
	    pix_multiply_1x128 (
		pix_multiply_1x128 (
		    unpack_32_1x128 (s), unpack_32_1x128 (m)),
		expand_alpha_1x128 (unpack_32_1x128 (d))));

	w--;
    }
}

static void
sse2_combine_in_reverse_ca (pixman_implementation_t *imp,
                            pixman_op_t              op,
                            uint32_t *               pd,
                            const uint32_t *         ps,
                            const uint32_t *         pm,
                            int                      w)
{
    uint32_t s, m, d;

    __m128i xmm_alpha_lo, xmm_alpha_hi;
    __m128i xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_mask_lo, xmm_mask_hi;

    while (w && (unsigned long)pd & 15)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = pack_1x128_32 (
	    pix_multiply_1x128 (
		unpack_32_1x128 (d),
		pix_multiply_1x128 (unpack_32_1x128 (m),
				   expand_alpha_1x128 (unpack_32_1x128 (s)))));
	w--;
    }

    while (w >= 4)
    {
	xmm_dst_hi = load_128_aligned ((__m128i*)pd);
	xmm_src_hi = load_128_unaligned ((__m128i*)ps);
	xmm_mask_hi = load_128_unaligned ((__m128i*)pm);

	unpack_128_2x128 (xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);
	unpack_128_2x128 (xmm_src_hi, &xmm_src_lo, &xmm_src_hi);
	unpack_128_2x128 (xmm_mask_hi, &xmm_mask_lo, &xmm_mask_hi);

	expand_alpha_2x128 (xmm_src_lo, xmm_src_hi,
			    &xmm_alpha_lo, &xmm_alpha_hi);
	pix_multiply_2x128 (&xmm_mask_lo, &xmm_mask_hi,
			    &xmm_alpha_lo, &xmm_alpha_hi,
			    &xmm_alpha_lo, &xmm_alpha_hi);

	pix_multiply_2x128 (&xmm_dst_lo, &xmm_dst_hi,
			    &xmm_alpha_lo, &xmm_alpha_hi,
			    &xmm_dst_lo, &xmm_dst_hi);

	save_128_aligned (
	    (__m128i*)pd, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	ps += 4;
	pd += 4;
	pm += 4;
	w -= 4;
    }

    while (w)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = pack_1x128_32 (
	    pix_multiply_1x128 (
		unpack_32_1x128 (d),
		pix_multiply_1x128 (unpack_32_1x128 (m),
				   expand_alpha_1x128 (unpack_32_1x128 (s)))));
	w--;
    }
}

static void
sse2_combine_out_ca (pixman_implementation_t *imp,
                     pixman_op_t              op,
                     uint32_t *               pd,
                     const uint32_t *         ps,
                     const uint32_t *         pm,
                     int                      w)
{
    uint32_t s, m, d;

    __m128i xmm_alpha_lo, xmm_alpha_hi;
    __m128i xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_mask_lo, xmm_mask_hi;

    while (w && (unsigned long)pd & 15)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = pack_1x128_32 (
	    pix_multiply_1x128 (
		pix_multiply_1x128 (
		    unpack_32_1x128 (s), unpack_32_1x128 (m)),
		negate_1x128 (expand_alpha_1x128 (unpack_32_1x128 (d)))));
	w--;
    }

    while (w >= 4)
    {
	xmm_dst_hi = load_128_aligned ((__m128i*)pd);
	xmm_src_hi = load_128_unaligned ((__m128i*)ps);
	xmm_mask_hi = load_128_unaligned ((__m128i*)pm);

	unpack_128_2x128 (xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);
	unpack_128_2x128 (xmm_src_hi, &xmm_src_lo, &xmm_src_hi);
	unpack_128_2x128 (xmm_mask_hi, &xmm_mask_lo, &xmm_mask_hi);

	expand_alpha_2x128 (xmm_dst_lo, xmm_dst_hi,
			    &xmm_alpha_lo, &xmm_alpha_hi);
	negate_2x128 (xmm_alpha_lo, xmm_alpha_hi,
		      &xmm_alpha_lo, &xmm_alpha_hi);

	pix_multiply_2x128 (&xmm_src_lo, &xmm_src_hi,
			    &xmm_mask_lo, &xmm_mask_hi,
			    &xmm_dst_lo, &xmm_dst_hi);
	pix_multiply_2x128 (&xmm_dst_lo, &xmm_dst_hi,
			    &xmm_alpha_lo, &xmm_alpha_hi,
			    &xmm_dst_lo, &xmm_dst_hi);

	save_128_aligned (
	    (__m128i*)pd, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	ps += 4;
	pd += 4;
	pm += 4;
	w -= 4;
    }

    while (w)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = pack_1x128_32 (
	    pix_multiply_1x128 (
		pix_multiply_1x128 (
		    unpack_32_1x128 (s), unpack_32_1x128 (m)),
		negate_1x128 (expand_alpha_1x128 (unpack_32_1x128 (d)))));

	w--;
    }
}

static void
sse2_combine_out_reverse_ca (pixman_implementation_t *imp,
                             pixman_op_t              op,
                             uint32_t *               pd,
                             const uint32_t *         ps,
                             const uint32_t *         pm,
                             int                      w)
{
    uint32_t s, m, d;

    __m128i xmm_alpha_lo, xmm_alpha_hi;
    __m128i xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_mask_lo, xmm_mask_hi;

    while (w && (unsigned long)pd & 15)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = pack_1x128_32 (
	    pix_multiply_1x128 (
		unpack_32_1x128 (d),
		negate_1x128 (pix_multiply_1x128 (
				 unpack_32_1x128 (m),
				 expand_alpha_1x128 (unpack_32_1x128 (s))))));
	w--;
    }

    while (w >= 4)
    {
	xmm_dst_hi = load_128_aligned ((__m128i*)pd);
	xmm_src_hi = load_128_unaligned ((__m128i*)ps);
	xmm_mask_hi = load_128_unaligned ((__m128i*)pm);

	unpack_128_2x128 (xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);
	unpack_128_2x128 (xmm_src_hi, &xmm_src_lo, &xmm_src_hi);
	unpack_128_2x128 (xmm_mask_hi, &xmm_mask_lo, &xmm_mask_hi);

	expand_alpha_2x128 (xmm_src_lo, xmm_src_hi,
			    &xmm_alpha_lo, &xmm_alpha_hi);

	pix_multiply_2x128 (&xmm_mask_lo, &xmm_mask_hi,
			    &xmm_alpha_lo, &xmm_alpha_hi,
			    &xmm_mask_lo, &xmm_mask_hi);

	negate_2x128 (xmm_mask_lo, xmm_mask_hi,
		      &xmm_mask_lo, &xmm_mask_hi);

	pix_multiply_2x128 (&xmm_dst_lo, &xmm_dst_hi,
			    &xmm_mask_lo, &xmm_mask_hi,
			    &xmm_dst_lo, &xmm_dst_hi);

	save_128_aligned (
	    (__m128i*)pd, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	ps += 4;
	pd += 4;
	pm += 4;
	w -= 4;
    }

    while (w)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = pack_1x128_32 (
	    pix_multiply_1x128 (
		unpack_32_1x128 (d),
		negate_1x128 (pix_multiply_1x128 (
				 unpack_32_1x128 (m),
				 expand_alpha_1x128 (unpack_32_1x128 (s))))));
	w--;
    }
}

static force_inline uint32_t
core_combine_atop_ca_pixel_sse2 (uint32_t src,
                                 uint32_t mask,
                                 uint32_t dst)
{
    __m128i m = unpack_32_1x128 (mask);
    __m128i s = unpack_32_1x128 (src);
    __m128i d = unpack_32_1x128 (dst);
    __m128i sa = expand_alpha_1x128 (s);
    __m128i da = expand_alpha_1x128 (d);

    s = pix_multiply_1x128 (s, m);
    m = negate_1x128 (pix_multiply_1x128 (m, sa));

    return pack_1x128_32 (pix_add_multiply_1x128 (&d, &m, &s, &da));
}

static void
sse2_combine_atop_ca (pixman_implementation_t *imp,
                      pixman_op_t              op,
                      uint32_t *               pd,
                      const uint32_t *         ps,
                      const uint32_t *         pm,
                      int                      w)
{
    uint32_t s, m, d;

    __m128i xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_alpha_src_lo, xmm_alpha_src_hi;
    __m128i xmm_alpha_dst_lo, xmm_alpha_dst_hi;
    __m128i xmm_mask_lo, xmm_mask_hi;

    while (w && (unsigned long)pd & 15)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = core_combine_atop_ca_pixel_sse2 (s, m, d);
	w--;
    }

    while (w >= 4)
    {
	xmm_dst_hi = load_128_aligned ((__m128i*)pd);
	xmm_src_hi = load_128_unaligned ((__m128i*)ps);
	xmm_mask_hi = load_128_unaligned ((__m128i*)pm);

	unpack_128_2x128 (xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);
	unpack_128_2x128 (xmm_src_hi, &xmm_src_lo, &xmm_src_hi);
	unpack_128_2x128 (xmm_mask_hi, &xmm_mask_lo, &xmm_mask_hi);

	expand_alpha_2x128 (xmm_src_lo, xmm_src_hi,
			    &xmm_alpha_src_lo, &xmm_alpha_src_hi);
	expand_alpha_2x128 (xmm_dst_lo, xmm_dst_hi,
			    &xmm_alpha_dst_lo, &xmm_alpha_dst_hi);

	pix_multiply_2x128 (&xmm_src_lo, &xmm_src_hi,
			    &xmm_mask_lo, &xmm_mask_hi,
			    &xmm_src_lo, &xmm_src_hi);
	pix_multiply_2x128 (&xmm_mask_lo, &xmm_mask_hi,
			    &xmm_alpha_src_lo, &xmm_alpha_src_hi,
			    &xmm_mask_lo, &xmm_mask_hi);

	negate_2x128 (xmm_mask_lo, xmm_mask_hi, &xmm_mask_lo, &xmm_mask_hi);

	pix_add_multiply_2x128 (
	    &xmm_dst_lo, &xmm_dst_hi, &xmm_mask_lo, &xmm_mask_hi,
	    &xmm_src_lo, &xmm_src_hi, &xmm_alpha_dst_lo, &xmm_alpha_dst_hi,
	    &xmm_dst_lo, &xmm_dst_hi);

	save_128_aligned (
	    (__m128i*)pd, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	ps += 4;
	pd += 4;
	pm += 4;
	w -= 4;
    }

    while (w)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = core_combine_atop_ca_pixel_sse2 (s, m, d);
	w--;
    }
}

static force_inline uint32_t
core_combine_reverse_atop_ca_pixel_sse2 (uint32_t src,
                                         uint32_t mask,
                                         uint32_t dst)
{
    __m128i m = unpack_32_1x128 (mask);
    __m128i s = unpack_32_1x128 (src);
    __m128i d = unpack_32_1x128 (dst);

    __m128i da = negate_1x128 (expand_alpha_1x128 (d));
    __m128i sa = expand_alpha_1x128 (s);

    s = pix_multiply_1x128 (s, m);
    m = pix_multiply_1x128 (m, sa);

    return pack_1x128_32 (pix_add_multiply_1x128 (&d, &m, &s, &da));
}

static void
sse2_combine_atop_reverse_ca (pixman_implementation_t *imp,
                              pixman_op_t              op,
                              uint32_t *               pd,
                              const uint32_t *         ps,
                              const uint32_t *         pm,
                              int                      w)
{
    uint32_t s, m, d;

    __m128i xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_alpha_src_lo, xmm_alpha_src_hi;
    __m128i xmm_alpha_dst_lo, xmm_alpha_dst_hi;
    __m128i xmm_mask_lo, xmm_mask_hi;

    while (w && (unsigned long)pd & 15)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = core_combine_reverse_atop_ca_pixel_sse2 (s, m, d);
	w--;
    }

    while (w >= 4)
    {
	xmm_dst_hi = load_128_aligned ((__m128i*)pd);
	xmm_src_hi = load_128_unaligned ((__m128i*)ps);
	xmm_mask_hi = load_128_unaligned ((__m128i*)pm);

	unpack_128_2x128 (xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);
	unpack_128_2x128 (xmm_src_hi, &xmm_src_lo, &xmm_src_hi);
	unpack_128_2x128 (xmm_mask_hi, &xmm_mask_lo, &xmm_mask_hi);

	expand_alpha_2x128 (xmm_src_lo, xmm_src_hi,
			    &xmm_alpha_src_lo, &xmm_alpha_src_hi);
	expand_alpha_2x128 (xmm_dst_lo, xmm_dst_hi,
			    &xmm_alpha_dst_lo, &xmm_alpha_dst_hi);

	pix_multiply_2x128 (&xmm_src_lo, &xmm_src_hi,
			    &xmm_mask_lo, &xmm_mask_hi,
			    &xmm_src_lo, &xmm_src_hi);
	pix_multiply_2x128 (&xmm_mask_lo, &xmm_mask_hi,
			    &xmm_alpha_src_lo, &xmm_alpha_src_hi,
			    &xmm_mask_lo, &xmm_mask_hi);

	negate_2x128 (xmm_alpha_dst_lo, xmm_alpha_dst_hi,
		      &xmm_alpha_dst_lo, &xmm_alpha_dst_hi);

	pix_add_multiply_2x128 (
	    &xmm_dst_lo, &xmm_dst_hi, &xmm_mask_lo, &xmm_mask_hi,
	    &xmm_src_lo, &xmm_src_hi, &xmm_alpha_dst_lo, &xmm_alpha_dst_hi,
	    &xmm_dst_lo, &xmm_dst_hi);

	save_128_aligned (
	    (__m128i*)pd, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	ps += 4;
	pd += 4;
	pm += 4;
	w -= 4;
    }

    while (w)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = core_combine_reverse_atop_ca_pixel_sse2 (s, m, d);
	w--;
    }
}

static force_inline uint32_t
core_combine_xor_ca_pixel_sse2 (uint32_t src,
                                uint32_t mask,
                                uint32_t dst)
{
    __m128i a = unpack_32_1x128 (mask);
    __m128i s = unpack_32_1x128 (src);
    __m128i d = unpack_32_1x128 (dst);

    __m128i alpha_dst = negate_1x128 (pix_multiply_1x128 (
				       a, expand_alpha_1x128 (s)));
    __m128i dest      = pix_multiply_1x128 (s, a);
    __m128i alpha_src = negate_1x128 (expand_alpha_1x128 (d));

    return pack_1x128_32 (pix_add_multiply_1x128 (&d,
                                                &alpha_dst,
                                                &dest,
                                                &alpha_src));
}

static void
sse2_combine_xor_ca (pixman_implementation_t *imp,
                     pixman_op_t              op,
                     uint32_t *               pd,
                     const uint32_t *         ps,
                     const uint32_t *         pm,
                     int                      w)
{
    uint32_t s, m, d;

    __m128i xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_alpha_src_lo, xmm_alpha_src_hi;
    __m128i xmm_alpha_dst_lo, xmm_alpha_dst_hi;
    __m128i xmm_mask_lo, xmm_mask_hi;

    while (w && (unsigned long)pd & 15)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = core_combine_xor_ca_pixel_sse2 (s, m, d);
	w--;
    }

    while (w >= 4)
    {
	xmm_dst_hi = load_128_aligned ((__m128i*)pd);
	xmm_src_hi = load_128_unaligned ((__m128i*)ps);
	xmm_mask_hi = load_128_unaligned ((__m128i*)pm);

	unpack_128_2x128 (xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);
	unpack_128_2x128 (xmm_src_hi, &xmm_src_lo, &xmm_src_hi);
	unpack_128_2x128 (xmm_mask_hi, &xmm_mask_lo, &xmm_mask_hi);

	expand_alpha_2x128 (xmm_src_lo, xmm_src_hi,
			    &xmm_alpha_src_lo, &xmm_alpha_src_hi);
	expand_alpha_2x128 (xmm_dst_lo, xmm_dst_hi,
			    &xmm_alpha_dst_lo, &xmm_alpha_dst_hi);

	pix_multiply_2x128 (&xmm_src_lo, &xmm_src_hi,
			    &xmm_mask_lo, &xmm_mask_hi,
			    &xmm_src_lo, &xmm_src_hi);
	pix_multiply_2x128 (&xmm_mask_lo, &xmm_mask_hi,
			    &xmm_alpha_src_lo, &xmm_alpha_src_hi,
			    &xmm_mask_lo, &xmm_mask_hi);

	negate_2x128 (xmm_alpha_dst_lo, xmm_alpha_dst_hi,
		      &xmm_alpha_dst_lo, &xmm_alpha_dst_hi);
	negate_2x128 (xmm_mask_lo, xmm_mask_hi,
		      &xmm_mask_lo, &xmm_mask_hi);

	pix_add_multiply_2x128 (
	    &xmm_dst_lo, &xmm_dst_hi, &xmm_mask_lo, &xmm_mask_hi,
	    &xmm_src_lo, &xmm_src_hi, &xmm_alpha_dst_lo, &xmm_alpha_dst_hi,
	    &xmm_dst_lo, &xmm_dst_hi);

	save_128_aligned (
	    (__m128i*)pd, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	ps += 4;
	pd += 4;
	pm += 4;
	w -= 4;
    }

    while (w)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = core_combine_xor_ca_pixel_sse2 (s, m, d);
	w--;
    }
}

static void
sse2_combine_add_ca (pixman_implementation_t *imp,
                     pixman_op_t              op,
                     uint32_t *               pd,
                     const uint32_t *         ps,
                     const uint32_t *         pm,
                     int                      w)
{
    uint32_t s, m, d;

    __m128i xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_mask_lo, xmm_mask_hi;

    while (w && (unsigned long)pd & 15)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = pack_1x128_32 (
	    _mm_adds_epu8 (pix_multiply_1x128 (unpack_32_1x128 (s),
					       unpack_32_1x128 (m)),
			   unpack_32_1x128 (d)));
	w--;
    }

    while (w >= 4)
    {
	xmm_src_hi = load_128_unaligned ((__m128i*)ps);
	xmm_mask_hi = load_128_unaligned ((__m128i*)pm);
	xmm_dst_hi = load_128_aligned ((__m128i*)pd);

	unpack_128_2x128 (xmm_src_hi, &xmm_src_lo, &xmm_src_hi);
	unpack_128_2x128 (xmm_mask_hi, &xmm_mask_lo, &xmm_mask_hi);
	unpack_128_2x128 (xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);

	pix_multiply_2x128 (&xmm_src_lo, &xmm_src_hi,
			    &xmm_mask_lo, &xmm_mask_hi,
			    &xmm_src_lo, &xmm_src_hi);

	save_128_aligned (
	    (__m128i*)pd, pack_2x128_128 (
		_mm_adds_epu8 (xmm_src_lo, xmm_dst_lo),
		_mm_adds_epu8 (xmm_src_hi, xmm_dst_hi)));

	ps += 4;
	pd += 4;
	pm += 4;
	w -= 4;
    }

    while (w)
    {
	s = *ps++;
	m = *pm++;
	d = *pd;

	*pd++ = pack_1x128_32 (
	    _mm_adds_epu8 (pix_multiply_1x128 (unpack_32_1x128 (s),
					       unpack_32_1x128 (m)),
			   unpack_32_1x128 (d)));
	w--;
    }
}

static force_inline __m128i
create_mask_16_128 (uint16_t mask)
{
    return _mm_set1_epi16 (mask);
}

/* Work around a code generation bug in Sun Studio 12. */
#if defined(__SUNPRO_C) && (__SUNPRO_C >= 0x590)
# define create_mask_2x32_128(mask0, mask1)				\
    (_mm_set_epi32 ((mask0), (mask1), (mask0), (mask1)))
#else
static force_inline __m128i
create_mask_2x32_128 (uint32_t mask0,
                      uint32_t mask1)
{
    return _mm_set_epi32 (mask0, mask1, mask0, mask1);
}
#endif

static void
sse2_composite_over_n_8888 (pixman_implementation_t *imp,
                            pixman_op_t              op,
                            pixman_image_t *         src_image,
                            pixman_image_t *         mask_image,
                            pixman_image_t *         dst_image,
                            int32_t                  src_x,
                            int32_t                  src_y,
                            int32_t                  mask_x,
                            int32_t                  mask_y,
                            int32_t                  dest_x,
                            int32_t                  dest_y,
                            int32_t                  width,
                            int32_t                  height)
{
    uint32_t src;
    uint32_t    *dst_line, *dst, d;
    int32_t w;
    int dst_stride;
    __m128i xmm_src, xmm_alpha;
    __m128i xmm_dst, xmm_dst_lo, xmm_dst_hi;

    src = _pixman_image_get_solid (imp, src_image, dst_image->bits.format);

    if (src == 0)
	return;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint32_t, dst_stride, dst_line, 1);

    xmm_src = expand_pixel_32_1x128 (src);
    xmm_alpha = expand_alpha_1x128 (xmm_src);

    while (height--)
    {
	dst = dst_line;

	dst_line += dst_stride;
	w = width;

	while (w && (unsigned long)dst & 15)
	{
	    d = *dst;
	    *dst++ = pack_1x128_32 (over_1x128 (xmm_src,
						xmm_alpha,
						unpack_32_1x128 (d)));
	    w--;
	}

	while (w >= 4)
	{
	    xmm_dst = load_128_aligned ((__m128i*)dst);

	    unpack_128_2x128 (xmm_dst, &xmm_dst_lo, &xmm_dst_hi);

	    over_2x128 (&xmm_src, &xmm_src,
			&xmm_alpha, &xmm_alpha,
			&xmm_dst_lo, &xmm_dst_hi);

	    /* rebuid the 4 pixel data and save*/
	    save_128_aligned (
		(__m128i*)dst, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	    w -= 4;
	    dst += 4;
	}

	while (w)
	{
	    d = *dst;
	    *dst++ = pack_1x128_32 (over_1x128 (xmm_src,
						xmm_alpha,
						unpack_32_1x128 (d)));
	    w--;
	}

    }
}

static void
sse2_composite_over_n_0565 (pixman_implementation_t *imp,
                            pixman_op_t              op,
                            pixman_image_t *         src_image,
                            pixman_image_t *         mask_image,
                            pixman_image_t *         dst_image,
                            int32_t                  src_x,
                            int32_t                  src_y,
                            int32_t                  mask_x,
                            int32_t                  mask_y,
                            int32_t                  dest_x,
                            int32_t                  dest_y,
                            int32_t                  width,
                            int32_t                  height)
{
    uint32_t src;
    uint16_t    *dst_line, *dst, d;
    int32_t w;
    int dst_stride;
    __m128i xmm_src, xmm_alpha;
    __m128i xmm_dst, xmm_dst0, xmm_dst1, xmm_dst2, xmm_dst3;

    src = _pixman_image_get_solid (imp, src_image, dst_image->bits.format);

    if (src == 0)
	return;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint16_t, dst_stride, dst_line, 1);

    xmm_src = expand_pixel_32_1x128 (src);
    xmm_alpha = expand_alpha_1x128 (xmm_src);

    while (height--)
    {
	dst = dst_line;

	dst_line += dst_stride;
	w = width;

	while (w && (unsigned long)dst & 15)
	{
	    d = *dst;

	    *dst++ = pack_565_32_16 (
		pack_1x128_32 (over_1x128 (xmm_src,
					   xmm_alpha,
					   expand565_16_1x128 (d))));
	    w--;
	}

	while (w >= 8)
	{
	    xmm_dst = load_128_aligned ((__m128i*)dst);

	    unpack_565_128_4x128 (xmm_dst,
				  &xmm_dst0, &xmm_dst1, &xmm_dst2, &xmm_dst3);

	    over_2x128 (&xmm_src, &xmm_src,
			&xmm_alpha, &xmm_alpha,
			&xmm_dst0, &xmm_dst1);
	    over_2x128 (&xmm_src, &xmm_src,
			&xmm_alpha, &xmm_alpha,
			&xmm_dst2, &xmm_dst3);

	    xmm_dst = pack_565_4x128_128 (
		&xmm_dst0, &xmm_dst1, &xmm_dst2, &xmm_dst3);

	    save_128_aligned ((__m128i*)dst, xmm_dst);

	    dst += 8;
	    w -= 8;
	}

	while (w--)
	{
	    d = *dst;
	    *dst++ = pack_565_32_16 (
		pack_1x128_32 (over_1x128 (xmm_src, xmm_alpha,
					   expand565_16_1x128 (d))));
	}
    }

}

static void
sse2_composite_add_n_8888_8888_ca (pixman_implementation_t *imp,
				   pixman_op_t              op,
				   pixman_image_t *         src_image,
				   pixman_image_t *         mask_image,
				   pixman_image_t *         dst_image,
				   int32_t                  src_x,
				   int32_t                  src_y,
				   int32_t                  mask_x,
				   int32_t                  mask_y,
				   int32_t                  dest_x,
				   int32_t                  dest_y,
				   int32_t                  width,
				   int32_t                  height)
{
    uint32_t src, srca;
    uint32_t    *dst_line, d;
    uint32_t    *mask_line, m;
    uint32_t pack_cmp;
    int dst_stride, mask_stride;

    __m128i xmm_src, xmm_alpha;
    __m128i xmm_dst;
    __m128i xmm_mask, xmm_mask_lo, xmm_mask_hi;

    __m128i mmx_src, mmx_alpha, mmx_mask, mmx_dest;

    src = _pixman_image_get_solid (imp, src_image, dst_image->bits.format);
    srca = src >> 24;

    if (src == 0)
	return;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint32_t, dst_stride, dst_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	mask_image, mask_x, mask_y, uint32_t, mask_stride, mask_line, 1);

    xmm_src = _mm_unpacklo_epi8 (
	create_mask_2x32_128 (src, src), _mm_setzero_si128 ());
    xmm_alpha = expand_alpha_1x128 (xmm_src);
    mmx_src   = xmm_src;
    mmx_alpha = xmm_alpha;

    while (height--)
    {
	int w = width;
	const uint32_t *pm = (uint32_t *)mask_line;
	uint32_t *pd = (uint32_t *)dst_line;

	dst_line += dst_stride;
	mask_line += mask_stride;

	while (w && (unsigned long)pd & 15)
	{
	    m = *pm++;

	    if (m)
	    {
		d = *pd;

		mmx_mask = unpack_32_1x128 (m);
		mmx_dest = unpack_32_1x128 (d);

		*pd = pack_1x128_32 (
		    _mm_adds_epu8 (pix_multiply_1x128 (mmx_mask, mmx_src),
				   mmx_dest));
	    }

	    pd++;
	    w--;
	}

	while (w >= 4)
	{
	    xmm_mask = load_128_unaligned ((__m128i*)pm);

	    pack_cmp =
		_mm_movemask_epi8 (
		    _mm_cmpeq_epi32 (xmm_mask, _mm_setzero_si128 ()));

	    /* if all bits in mask are zero, pack_cmp are equal to 0xffff */
	    if (pack_cmp != 0xffff)
	    {
		xmm_dst = load_128_aligned ((__m128i*)pd);

		unpack_128_2x128 (xmm_mask, &xmm_mask_lo, &xmm_mask_hi);

		pix_multiply_2x128 (&xmm_src, &xmm_src,
				    &xmm_mask_lo, &xmm_mask_hi,
				    &xmm_mask_lo, &xmm_mask_hi);
		xmm_mask_hi = pack_2x128_128 (xmm_mask_lo, xmm_mask_hi);

		save_128_aligned (
		    (__m128i*)pd, _mm_adds_epu8 (xmm_mask_hi, xmm_dst));
	    }

	    pd += 4;
	    pm += 4;
	    w -= 4;
	}

	while (w)
	{
	    m = *pm++;

	    if (m)
	    {
		d = *pd;

		mmx_mask = unpack_32_1x128 (m);
		mmx_dest = unpack_32_1x128 (d);

		*pd = pack_1x128_32 (
		    _mm_adds_epu8 (pix_multiply_1x128 (mmx_mask, mmx_src),
				   mmx_dest));
	    }

	    pd++;
	    w--;
	}
    }

}

static void
sse2_composite_over_n_8888_8888_ca (pixman_implementation_t *imp,
                                    pixman_op_t              op,
                                    pixman_image_t *         src_image,
                                    pixman_image_t *         mask_image,
                                    pixman_image_t *         dst_image,
                                    int32_t                  src_x,
                                    int32_t                  src_y,
                                    int32_t                  mask_x,
                                    int32_t                  mask_y,
                                    int32_t                  dest_x,
                                    int32_t                  dest_y,
                                    int32_t                  width,
                                    int32_t                  height)
{
    uint32_t src;
    uint32_t    *dst_line, d;
    uint32_t    *mask_line, m;
    uint32_t pack_cmp;
    int dst_stride, mask_stride;

    __m128i xmm_src, xmm_alpha;
    __m128i xmm_dst, xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_mask, xmm_mask_lo, xmm_mask_hi;

    __m128i mmx_src, mmx_alpha, mmx_mask, mmx_dest;

    src = _pixman_image_get_solid (imp, src_image, dst_image->bits.format);

    if (src == 0)
	return;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint32_t, dst_stride, dst_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	mask_image, mask_x, mask_y, uint32_t, mask_stride, mask_line, 1);

    xmm_src = _mm_unpacklo_epi8 (
	create_mask_2x32_128 (src, src), _mm_setzero_si128 ());
    xmm_alpha = expand_alpha_1x128 (xmm_src);
    mmx_src   = xmm_src;
    mmx_alpha = xmm_alpha;

    while (height--)
    {
	int w = width;
	const uint32_t *pm = (uint32_t *)mask_line;
	uint32_t *pd = (uint32_t *)dst_line;

	dst_line += dst_stride;
	mask_line += mask_stride;

	while (w && (unsigned long)pd & 15)
	{
	    m = *pm++;

	    if (m)
	    {
		d = *pd;
		mmx_mask = unpack_32_1x128 (m);
		mmx_dest = unpack_32_1x128 (d);

		*pd = pack_1x128_32 (in_over_1x128 (&mmx_src,
		                                  &mmx_alpha,
		                                  &mmx_mask,
		                                  &mmx_dest));
	    }

	    pd++;
	    w--;
	}

	while (w >= 4)
	{
	    xmm_mask = load_128_unaligned ((__m128i*)pm);

	    pack_cmp =
		_mm_movemask_epi8 (
		    _mm_cmpeq_epi32 (xmm_mask, _mm_setzero_si128 ()));

	    /* if all bits in mask are zero, pack_cmp are equal to 0xffff */
	    if (pack_cmp != 0xffff)
	    {
		xmm_dst = load_128_aligned ((__m128i*)pd);

		unpack_128_2x128 (xmm_mask, &xmm_mask_lo, &xmm_mask_hi);
		unpack_128_2x128 (xmm_dst, &xmm_dst_lo, &xmm_dst_hi);

		in_over_2x128 (&xmm_src, &xmm_src,
			       &xmm_alpha, &xmm_alpha,
			       &xmm_mask_lo, &xmm_mask_hi,
			       &xmm_dst_lo, &xmm_dst_hi);

		save_128_aligned (
		    (__m128i*)pd, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));
	    }

	    pd += 4;
	    pm += 4;
	    w -= 4;
	}

	while (w)
	{
	    m = *pm++;

	    if (m)
	    {
		d = *pd;
		mmx_mask = unpack_32_1x128 (m);
		mmx_dest = unpack_32_1x128 (d);

		*pd = pack_1x128_32 (
		    in_over_1x128 (&mmx_src, &mmx_alpha, &mmx_mask, &mmx_dest));
	    }

	    pd++;
	    w--;
	}
    }

}

static void
sse2_composite_over_8888_n_8888 (pixman_implementation_t *imp,
                                 pixman_op_t              op,
                                 pixman_image_t *         src_image,
                                 pixman_image_t *         mask_image,
                                 pixman_image_t *         dst_image,
                                 int32_t                  src_x,
                                 int32_t                  src_y,
                                 int32_t                  mask_x,
                                 int32_t                  mask_y,
                                 int32_t                  dest_x,
                                 int32_t                  dest_y,
                                 int32_t                  width,
                                 int32_t                  height)
{
    uint32_t    *dst_line, *dst;
    uint32_t    *src_line, *src;
    uint32_t mask;
    int32_t w;
    int dst_stride, src_stride;

    __m128i xmm_mask;
    __m128i xmm_src, xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst, xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_alpha_lo, xmm_alpha_hi;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint32_t, dst_stride, dst_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	src_image, src_x, src_y, uint32_t, src_stride, src_line, 1);

    mask = _pixman_image_get_solid (imp, mask_image, PIXMAN_a8r8g8b8);

    xmm_mask = create_mask_16_128 (mask >> 24);

    while (height--)
    {
	dst = dst_line;
	dst_line += dst_stride;
	src = src_line;
	src_line += src_stride;
	w = width;

	while (w && (unsigned long)dst & 15)
	{
	    uint32_t s = *src++;

	    if (s)
	    {
		uint32_t d = *dst;
		
		__m128i ms = unpack_32_1x128 (s);
		__m128i alpha    = expand_alpha_1x128 (ms);
		__m128i dest     = xmm_mask;
		__m128i alpha_dst = unpack_32_1x128 (d);
		
		*dst = pack_1x128_32 (
		    in_over_1x128 (&ms, &alpha, &dest, &alpha_dst));
	    }
	    dst++;
	    w--;
	}

	while (w >= 4)
	{
	    xmm_src = load_128_unaligned ((__m128i*)src);

	    if (!is_zero (xmm_src))
	    {
		xmm_dst = load_128_aligned ((__m128i*)dst);
		
		unpack_128_2x128 (xmm_src, &xmm_src_lo, &xmm_src_hi);
		unpack_128_2x128 (xmm_dst, &xmm_dst_lo, &xmm_dst_hi);
		expand_alpha_2x128 (xmm_src_lo, xmm_src_hi,
				    &xmm_alpha_lo, &xmm_alpha_hi);
		
		in_over_2x128 (&xmm_src_lo, &xmm_src_hi,
			       &xmm_alpha_lo, &xmm_alpha_hi,
			       &xmm_mask, &xmm_mask,
			       &xmm_dst_lo, &xmm_dst_hi);
		
		save_128_aligned (
		    (__m128i*)dst, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));
	    }
		
	    dst += 4;
	    src += 4;
	    w -= 4;
	}

	while (w)
	{
	    uint32_t s = *src++;

	    if (s)
	    {
		uint32_t d = *dst;
		
		__m128i ms = unpack_32_1x128 (s);
		__m128i alpha = expand_alpha_1x128 (ms);
		__m128i mask  = xmm_mask;
		__m128i dest  = unpack_32_1x128 (d);
		
		*dst = pack_1x128_32 (
		    in_over_1x128 (&ms, &alpha, &mask, &dest));
	    }

	    dst++;
	    w--;
	}
    }

}

static void
sse2_composite_src_x888_8888 (pixman_implementation_t *imp,
			      pixman_op_t              op,
			      pixman_image_t *         src_image,
			      pixman_image_t *         mask_image,
			      pixman_image_t *         dst_image,
			      int32_t                  src_x,
			      int32_t                  src_y,
			      int32_t                  mask_x,
			      int32_t                  mask_y,
			      int32_t                  dest_x,
			      int32_t                  dest_y,
			      int32_t                  width,
			      int32_t                  height)
{
    uint32_t    *dst_line, *dst;
    uint32_t    *src_line, *src;
    int32_t w;
    int dst_stride, src_stride;


    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint32_t, dst_stride, dst_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	src_image, src_x, src_y, uint32_t, src_stride, src_line, 1);

    while (height--)
    {
	dst = dst_line;
	dst_line += dst_stride;
	src = src_line;
	src_line += src_stride;
	w = width;

	while (w && (unsigned long)dst & 15)
	{
	    *dst++ = *src++ | 0xff000000;
	    w--;
	}

	while (w >= 16)
	{
	    __m128i xmm_src1, xmm_src2, xmm_src3, xmm_src4;
	    
	    xmm_src1 = load_128_unaligned ((__m128i*)src + 0);
	    xmm_src2 = load_128_unaligned ((__m128i*)src + 1);
	    xmm_src3 = load_128_unaligned ((__m128i*)src + 2);
	    xmm_src4 = load_128_unaligned ((__m128i*)src + 3);
	    
	    save_128_aligned ((__m128i*)dst + 0, _mm_or_si128 (xmm_src1, mask_ff000000));
	    save_128_aligned ((__m128i*)dst + 1, _mm_or_si128 (xmm_src2, mask_ff000000));
	    save_128_aligned ((__m128i*)dst + 2, _mm_or_si128 (xmm_src3, mask_ff000000));
	    save_128_aligned ((__m128i*)dst + 3, _mm_or_si128 (xmm_src4, mask_ff000000));
	    
	    dst += 16;
	    src += 16;
	    w -= 16;
	}

	while (w)
	{
	    *dst++ = *src++ | 0xff000000;
	    w--;
	}
    }

}

static void
sse2_composite_over_x888_n_8888 (pixman_implementation_t *imp,
                                 pixman_op_t              op,
                                 pixman_image_t *         src_image,
                                 pixman_image_t *         mask_image,
                                 pixman_image_t *         dst_image,
                                 int32_t                  src_x,
                                 int32_t                  src_y,
                                 int32_t                  mask_x,
                                 int32_t                  mask_y,
                                 int32_t                  dest_x,
                                 int32_t                  dest_y,
                                 int32_t                  width,
                                 int32_t                  height)
{
    uint32_t    *dst_line, *dst;
    uint32_t    *src_line, *src;
    uint32_t mask;
    int dst_stride, src_stride;
    int32_t w;

    __m128i xmm_mask, xmm_alpha;
    __m128i xmm_src, xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst, xmm_dst_lo, xmm_dst_hi;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint32_t, dst_stride, dst_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	src_image, src_x, src_y, uint32_t, src_stride, src_line, 1);

    mask = _pixman_image_get_solid (imp, mask_image, PIXMAN_a8r8g8b8);

    xmm_mask = create_mask_16_128 (mask >> 24);
    xmm_alpha = mask_00ff;

    while (height--)
    {
	dst = dst_line;
	dst_line += dst_stride;
	src = src_line;
	src_line += src_stride;
	w = width;

	while (w && (unsigned long)dst & 15)
	{
	    uint32_t s = (*src++) | 0xff000000;
	    uint32_t d = *dst;

	    __m128i src   = unpack_32_1x128 (s);
	    __m128i alpha = xmm_alpha;
	    __m128i mask  = xmm_mask;
	    __m128i dest  = unpack_32_1x128 (d);

	    *dst++ = pack_1x128_32 (
		in_over_1x128 (&src, &alpha, &mask, &dest));

	    w--;
	}

	while (w >= 4)
	{
	    xmm_src = _mm_or_si128 (
		load_128_unaligned ((__m128i*)src), mask_ff000000);
	    xmm_dst = load_128_aligned ((__m128i*)dst);

	    unpack_128_2x128 (xmm_src, &xmm_src_lo, &xmm_src_hi);
	    unpack_128_2x128 (xmm_dst, &xmm_dst_lo, &xmm_dst_hi);

	    in_over_2x128 (&xmm_src_lo, &xmm_src_hi,
			   &xmm_alpha, &xmm_alpha,
			   &xmm_mask, &xmm_mask,
			   &xmm_dst_lo, &xmm_dst_hi);

	    save_128_aligned (
		(__m128i*)dst, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	    dst += 4;
	    src += 4;
	    w -= 4;

	}

	while (w)
	{
	    uint32_t s = (*src++) | 0xff000000;
	    uint32_t d = *dst;

	    __m128i src  = unpack_32_1x128 (s);
	    __m128i alpha = xmm_alpha;
	    __m128i mask  = xmm_mask;
	    __m128i dest  = unpack_32_1x128 (d);

	    *dst++ = pack_1x128_32 (
		in_over_1x128 (&src, &alpha, &mask, &dest));

	    w--;
	}
    }

}

static void
sse2_composite_over_8888_8888 (pixman_implementation_t *imp,
                               pixman_op_t              op,
                               pixman_image_t *         src_image,
                               pixman_image_t *         mask_image,
                               pixman_image_t *         dst_image,
                               int32_t                  src_x,
                               int32_t                  src_y,
                               int32_t                  mask_x,
                               int32_t                  mask_y,
                               int32_t                  dest_x,
                               int32_t                  dest_y,
                               int32_t                  width,
                               int32_t                  height)
{
    int dst_stride, src_stride;
    uint32_t    *dst_line, *dst;
    uint32_t    *src_line, *src;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint32_t, dst_stride, dst_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	src_image, src_x, src_y, uint32_t, src_stride, src_line, 1);

    dst = dst_line;
    src = src_line;

    while (height--)
    {
	sse2_combine_over_u (imp, op, dst, src, NULL, width);

	dst += dst_stride;
	src += src_stride;
    }
}

static force_inline uint16_t
composite_over_8888_0565pixel (uint32_t src, uint16_t dst)
{
    __m128i ms;

    ms = unpack_32_1x128 (src);
    return pack_565_32_16 (
	pack_1x128_32 (
	    over_1x128 (
		ms, expand_alpha_1x128 (ms), expand565_16_1x128 (dst))));
}

static void
sse2_composite_over_8888_0565 (pixman_implementation_t *imp,
                               pixman_op_t              op,
                               pixman_image_t *         src_image,
                               pixman_image_t *         mask_image,
                               pixman_image_t *         dst_image,
                               int32_t                  src_x,
                               int32_t                  src_y,
                               int32_t                  mask_x,
                               int32_t                  mask_y,
                               int32_t                  dest_x,
                               int32_t                  dest_y,
                               int32_t                  width,
                               int32_t                  height)
{
    uint16_t    *dst_line, *dst, d;
    uint32_t    *src_line, *src, s;
    int dst_stride, src_stride;
    int32_t w;

    __m128i xmm_alpha_lo, xmm_alpha_hi;
    __m128i xmm_src, xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst, xmm_dst0, xmm_dst1, xmm_dst2, xmm_dst3;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint16_t, dst_stride, dst_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	src_image, src_x, src_y, uint32_t, src_stride, src_line, 1);

    while (height--)
    {
	dst = dst_line;
	src = src_line;

	dst_line += dst_stride;
	src_line += src_stride;
	w = width;

	/* Align dst on a 16-byte boundary */
	while (w &&
	       ((unsigned long)dst & 15))
	{
	    s = *src++;
	    d = *dst;

	    *dst++ = composite_over_8888_0565pixel (s, d);
	    w--;
	}

	/* It's a 8 pixel loop */
	while (w >= 8)
	{
	    /* I'm loading unaligned because I'm not sure
	     * about the address alignment.
	     */
	    xmm_src = load_128_unaligned ((__m128i*) src);
	    xmm_dst = load_128_aligned ((__m128i*) dst);

	    /* Unpacking */
	    unpack_128_2x128 (xmm_src, &xmm_src_lo, &xmm_src_hi);
	    unpack_565_128_4x128 (xmm_dst,
				  &xmm_dst0, &xmm_dst1, &xmm_dst2, &xmm_dst3);
	    expand_alpha_2x128 (xmm_src_lo, xmm_src_hi,
				&xmm_alpha_lo, &xmm_alpha_hi);

	    /* I'm loading next 4 pixels from memory
	     * before to optimze the memory read.
	     */
	    xmm_src = load_128_unaligned ((__m128i*) (src + 4));

	    over_2x128 (&xmm_src_lo, &xmm_src_hi,
			&xmm_alpha_lo, &xmm_alpha_hi,
			&xmm_dst0, &xmm_dst1);

	    /* Unpacking */
	    unpack_128_2x128 (xmm_src, &xmm_src_lo, &xmm_src_hi);
	    expand_alpha_2x128 (xmm_src_lo, xmm_src_hi,
				&xmm_alpha_lo, &xmm_alpha_hi);

	    over_2x128 (&xmm_src_lo, &xmm_src_hi,
			&xmm_alpha_lo, &xmm_alpha_hi,
			&xmm_dst2, &xmm_dst3);

	    save_128_aligned (
		(__m128i*)dst, pack_565_4x128_128 (
		    &xmm_dst0, &xmm_dst1, &xmm_dst2, &xmm_dst3));

	    w -= 8;
	    dst += 8;
	    src += 8;
	}

	while (w--)
	{
	    s = *src++;
	    d = *dst;

	    *dst++ = composite_over_8888_0565pixel (s, d);
	}
    }

}

static void
sse2_composite_over_n_8_8888 (pixman_implementation_t *imp,
                              pixman_op_t              op,
                              pixman_image_t *         src_image,
                              pixman_image_t *         mask_image,
                              pixman_image_t *         dst_image,
                              int32_t                  src_x,
                              int32_t                  src_y,
                              int32_t                  mask_x,
                              int32_t                  mask_y,
                              int32_t                  dest_x,
                              int32_t                  dest_y,
                              int32_t                  width,
                              int32_t                  height)
{
    uint32_t src, srca;
    uint32_t *dst_line, *dst;
    uint8_t *mask_line, *mask;
    int dst_stride, mask_stride;
    int32_t w;
    uint32_t m, d;

    __m128i xmm_src, xmm_alpha, xmm_def;
    __m128i xmm_dst, xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_mask, xmm_mask_lo, xmm_mask_hi;

    __m128i mmx_src, mmx_alpha, mmx_mask, mmx_dest;

    src = _pixman_image_get_solid (imp, src_image, dst_image->bits.format);

    srca = src >> 24;
    if (src == 0)
	return;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint32_t, dst_stride, dst_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	mask_image, mask_x, mask_y, uint8_t, mask_stride, mask_line, 1);

    xmm_def = create_mask_2x32_128 (src, src);
    xmm_src = expand_pixel_32_1x128 (src);
    xmm_alpha = expand_alpha_1x128 (xmm_src);
    mmx_src   = xmm_src;
    mmx_alpha = xmm_alpha;

    while (height--)
    {
	dst = dst_line;
	dst_line += dst_stride;
	mask = mask_line;
	mask_line += mask_stride;
	w = width;

	while (w && (unsigned long)dst & 15)
	{
	    uint8_t m = *mask++;

	    if (m)
	    {
		d = *dst;
		mmx_mask = expand_pixel_8_1x128 (m);
		mmx_dest = unpack_32_1x128 (d);

		*dst = pack_1x128_32 (in_over_1x128 (&mmx_src,
		                                   &mmx_alpha,
		                                   &mmx_mask,
		                                   &mmx_dest));
	    }

	    w--;
	    dst++;
	}

	while (w >= 4)
	{
	    m = *((uint32_t*)mask);

	    if (srca == 0xff && m == 0xffffffff)
	    {
		save_128_aligned ((__m128i*)dst, xmm_def);
	    }
	    else if (m)
	    {
		xmm_dst = load_128_aligned ((__m128i*) dst);
		xmm_mask = unpack_32_1x128 (m);
		xmm_mask = _mm_unpacklo_epi8 (xmm_mask, _mm_setzero_si128 ());

		/* Unpacking */
		unpack_128_2x128 (xmm_dst, &xmm_dst_lo, &xmm_dst_hi);
		unpack_128_2x128 (xmm_mask, &xmm_mask_lo, &xmm_mask_hi);

		expand_alpha_rev_2x128 (xmm_mask_lo, xmm_mask_hi,
					&xmm_mask_lo, &xmm_mask_hi);

		in_over_2x128 (&xmm_src, &xmm_src,
			       &xmm_alpha, &xmm_alpha,
			       &xmm_mask_lo, &xmm_mask_hi,
			       &xmm_dst_lo, &xmm_dst_hi);

		save_128_aligned (
		    (__m128i*)dst, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));
	    }

	    w -= 4;
	    dst += 4;
	    mask += 4;
	}

	while (w)
	{
	    uint8_t m = *mask++;

	    if (m)
	    {
		d = *dst;
		mmx_mask = expand_pixel_8_1x128 (m);
		mmx_dest = unpack_32_1x128 (d);

		*dst = pack_1x128_32 (in_over_1x128 (&mmx_src,
		                                   &mmx_alpha,
		                                   &mmx_mask,
		                                   &mmx_dest));
	    }

	    w--;
	    dst++;
	}
    }

}

static pixman_bool_t
pixman_fill_sse2 (uint32_t *bits,
                  int       stride,
                  int       bpp,
                  int       x,
                  int       y,
                  int       width,
                  int       height,
                  uint32_t  data)
{
    uint32_t byte_width;
    uint8_t         *byte_line;

    __m128i xmm_def;

    if (bpp == 8)
    {
	uint8_t b;
	uint16_t w;

	stride = stride * (int) sizeof (uint32_t) / 1;
	byte_line = (uint8_t *)(((uint8_t *)bits) + stride * y + x);
	byte_width = width;
	stride *= 1;

	b = data & 0xff;
	w = (b << 8) | b;
	data = (w << 16) | w;
    }
    else if (bpp == 16)
    {
	stride = stride * (int) sizeof (uint32_t) / 2;
	byte_line = (uint8_t *)(((uint16_t *)bits) + stride * y + x);
	byte_width = 2 * width;
	stride *= 2;

        data = (data & 0xffff) * 0x00010001;
    }
    else if (bpp == 32)
    {
	stride = stride * (int) sizeof (uint32_t) / 4;
	byte_line = (uint8_t *)(((uint32_t *)bits) + stride * y + x);
	byte_width = 4 * width;
	stride *= 4;
    }
    else
    {
	return FALSE;
    }

    xmm_def = create_mask_2x32_128 (data, data);

    while (height--)
    {
	int w;
	uint8_t *d = byte_line;
	byte_line += stride;
	w = byte_width;

	while (w >= 1 && ((unsigned long)d & 1))
	{
	    *(uint8_t *)d = data;
	    w -= 1;
	    d += 1;
	}

	while (w >= 2 && ((unsigned long)d & 3))
	{
	    *(uint16_t *)d = data;
	    w -= 2;
	    d += 2;
	}

	while (w >= 4 && ((unsigned long)d & 15))
	{
	    *(uint32_t *)d = data;

	    w -= 4;
	    d += 4;
	}

	while (w >= 128)
	{
	    save_128_aligned ((__m128i*)(d),     xmm_def);
	    save_128_aligned ((__m128i*)(d + 16),  xmm_def);
	    save_128_aligned ((__m128i*)(d + 32),  xmm_def);
	    save_128_aligned ((__m128i*)(d + 48),  xmm_def);
	    save_128_aligned ((__m128i*)(d + 64),  xmm_def);
	    save_128_aligned ((__m128i*)(d + 80),  xmm_def);
	    save_128_aligned ((__m128i*)(d + 96),  xmm_def);
	    save_128_aligned ((__m128i*)(d + 112), xmm_def);

	    d += 128;
	    w -= 128;
	}

	if (w >= 64)
	{
	    save_128_aligned ((__m128i*)(d),     xmm_def);
	    save_128_aligned ((__m128i*)(d + 16),  xmm_def);
	    save_128_aligned ((__m128i*)(d + 32),  xmm_def);
	    save_128_aligned ((__m128i*)(d + 48),  xmm_def);

	    d += 64;
	    w -= 64;
	}

	if (w >= 32)
	{
	    save_128_aligned ((__m128i*)(d),     xmm_def);
	    save_128_aligned ((__m128i*)(d + 16),  xmm_def);

	    d += 32;
	    w -= 32;
	}

	if (w >= 16)
	{
	    save_128_aligned ((__m128i*)(d),     xmm_def);

	    d += 16;
	    w -= 16;
	}

	while (w >= 4)
	{
	    *(uint32_t *)d = data;

	    w -= 4;
	    d += 4;
	}

	if (w >= 2)
	{
	    *(uint16_t *)d = data;
	    w -= 2;
	    d += 2;
	}

	if (w >= 1)
	{
	    *(uint8_t *)d = data;
	    w -= 1;
	    d += 1;
	}
    }

    return TRUE;
}

static void
sse2_composite_src_n_8_8888 (pixman_implementation_t *imp,
                             pixman_op_t              op,
                             pixman_image_t *         src_image,
                             pixman_image_t *         mask_image,
                             pixman_image_t *         dst_image,
                             int32_t                  src_x,
                             int32_t                  src_y,
                             int32_t                  mask_x,
                             int32_t                  mask_y,
                             int32_t                  dest_x,
                             int32_t                  dest_y,
                             int32_t                  width,
                             int32_t                  height)
{
    uint32_t src, srca;
    uint32_t    *dst_line, *dst;
    uint8_t     *mask_line, *mask;
    int dst_stride, mask_stride;
    int32_t w;
    uint32_t m;

    __m128i xmm_src, xmm_def;
    __m128i xmm_mask, xmm_mask_lo, xmm_mask_hi;

    src = _pixman_image_get_solid (imp, src_image, dst_image->bits.format);

    srca = src >> 24;
    if (src == 0)
    {
	pixman_fill_sse2 (dst_image->bits.bits, dst_image->bits.rowstride,
	                  PIXMAN_FORMAT_BPP (dst_image->bits.format),
	                  dest_x, dest_y, width, height, 0);
	return;
    }

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint32_t, dst_stride, dst_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	mask_image, mask_x, mask_y, uint8_t, mask_stride, mask_line, 1);

    xmm_def = create_mask_2x32_128 (src, src);
    xmm_src = expand_pixel_32_1x128 (src);

    while (height--)
    {
	dst = dst_line;
	dst_line += dst_stride;
	mask = mask_line;
	mask_line += mask_stride;
	w = width;

	while (w && (unsigned long)dst & 15)
	{
	    uint8_t m = *mask++;

	    if (m)
	    {
		*dst = pack_1x128_32 (
		    pix_multiply_1x128 (xmm_src, expand_pixel_8_1x128 (m)));
	    }
	    else
	    {
		*dst = 0;
	    }

	    w--;
	    dst++;
	}

	while (w >= 4)
	{
	    m = *((uint32_t*)mask);

	    if (srca == 0xff && m == 0xffffffff)
	    {
		save_128_aligned ((__m128i*)dst, xmm_def);
	    }
	    else if (m)
	    {
		xmm_mask = unpack_32_1x128 (m);
		xmm_mask = _mm_unpacklo_epi8 (xmm_mask, _mm_setzero_si128 ());

		/* Unpacking */
		unpack_128_2x128 (xmm_mask, &xmm_mask_lo, &xmm_mask_hi);

		expand_alpha_rev_2x128 (xmm_mask_lo, xmm_mask_hi,
					&xmm_mask_lo, &xmm_mask_hi);

		pix_multiply_2x128 (&xmm_src, &xmm_src,
				    &xmm_mask_lo, &xmm_mask_hi,
				    &xmm_mask_lo, &xmm_mask_hi);

		save_128_aligned (
		    (__m128i*)dst, pack_2x128_128 (xmm_mask_lo, xmm_mask_hi));
	    }
	    else
	    {
		save_128_aligned ((__m128i*)dst, _mm_setzero_si128 ());
	    }

	    w -= 4;
	    dst += 4;
	    mask += 4;
	}

	while (w)
	{
	    uint8_t m = *mask++;

	    if (m)
	    {
		*dst = pack_1x128_32 (
		    pix_multiply_1x128 (
			xmm_src, expand_pixel_8_1x128 (m)));
	    }
	    else
	    {
		*dst = 0;
	    }

	    w--;
	    dst++;
	}
    }

}

static void
sse2_composite_over_n_8_0565 (pixman_implementation_t *imp,
                              pixman_op_t              op,
                              pixman_image_t *         src_image,
                              pixman_image_t *         mask_image,
                              pixman_image_t *         dst_image,
                              int32_t                  src_x,
                              int32_t                  src_y,
                              int32_t                  mask_x,
                              int32_t                  mask_y,
                              int32_t                  dest_x,
                              int32_t                  dest_y,
                              int32_t                  width,
                              int32_t                  height)
{
    uint32_t src, srca;
    uint16_t    *dst_line, *dst, d;
    uint8_t     *mask_line, *mask;
    int dst_stride, mask_stride;
    int32_t w;
    uint32_t m;
    __m128i mmx_src, mmx_alpha, mmx_mask, mmx_dest;

    __m128i xmm_src, xmm_alpha;
    __m128i xmm_mask, xmm_mask_lo, xmm_mask_hi;
    __m128i xmm_dst, xmm_dst0, xmm_dst1, xmm_dst2, xmm_dst3;

    src = _pixman_image_get_solid (imp, src_image, dst_image->bits.format);

    srca = src >> 24;
    if (src == 0)
	return;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint16_t, dst_stride, dst_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	mask_image, mask_x, mask_y, uint8_t, mask_stride, mask_line, 1);

    xmm_src = expand_pixel_32_1x128 (src);
    xmm_alpha = expand_alpha_1x128 (xmm_src);
    mmx_src = xmm_src;
    mmx_alpha = xmm_alpha;

    while (height--)
    {
	dst = dst_line;
	dst_line += dst_stride;
	mask = mask_line;
	mask_line += mask_stride;
	w = width;

	while (w && (unsigned long)dst & 15)
	{
	    m = *mask++;

	    if (m)
	    {
		d = *dst;
		mmx_mask = expand_alpha_rev_1x128 (unpack_32_1x128 (m));
		mmx_dest = expand565_16_1x128 (d);

		*dst = pack_565_32_16 (
		    pack_1x128_32 (
			in_over_1x128 (
			    &mmx_src, &mmx_alpha, &mmx_mask, &mmx_dest)));
	    }

	    w--;
	    dst++;
	}

	while (w >= 8)
	{
	    xmm_dst = load_128_aligned ((__m128i*) dst);
	    unpack_565_128_4x128 (xmm_dst,
				  &xmm_dst0, &xmm_dst1, &xmm_dst2, &xmm_dst3);

	    m = *((uint32_t*)mask);
	    mask += 4;

	    if (m)
	    {
		xmm_mask = unpack_32_1x128 (m);
		xmm_mask = _mm_unpacklo_epi8 (xmm_mask, _mm_setzero_si128 ());

		/* Unpacking */
		unpack_128_2x128 (xmm_mask, &xmm_mask_lo, &xmm_mask_hi);

		expand_alpha_rev_2x128 (xmm_mask_lo, xmm_mask_hi,
					&xmm_mask_lo, &xmm_mask_hi);

		in_over_2x128 (&xmm_src, &xmm_src,
			       &xmm_alpha, &xmm_alpha,
			       &xmm_mask_lo, &xmm_mask_hi,
			       &xmm_dst0, &xmm_dst1);
	    }

	    m = *((uint32_t*)mask);
	    mask += 4;

	    if (m)
	    {
		xmm_mask = unpack_32_1x128 (m);
		xmm_mask = _mm_unpacklo_epi8 (xmm_mask, _mm_setzero_si128 ());

		/* Unpacking */
		unpack_128_2x128 (xmm_mask, &xmm_mask_lo, &xmm_mask_hi);

		expand_alpha_rev_2x128 (xmm_mask_lo, xmm_mask_hi,
					&xmm_mask_lo, &xmm_mask_hi);
		in_over_2x128 (&xmm_src, &xmm_src,
			       &xmm_alpha, &xmm_alpha,
			       &xmm_mask_lo, &xmm_mask_hi,
			       &xmm_dst2, &xmm_dst3);
	    }

	    save_128_aligned (
		(__m128i*)dst, pack_565_4x128_128 (
		    &xmm_dst0, &xmm_dst1, &xmm_dst2, &xmm_dst3));

	    w -= 8;
	    dst += 8;
	}

	while (w)
	{
	    m = *mask++;

	    if (m)
	    {
		d = *dst;
		mmx_mask = expand_alpha_rev_1x128 (unpack_32_1x128 (m));
		mmx_dest = expand565_16_1x128 (d);

		*dst = pack_565_32_16 (
		    pack_1x128_32 (
			in_over_1x128 (
			    &mmx_src, &mmx_alpha, &mmx_mask, &mmx_dest)));
	    }

	    w--;
	    dst++;
	}
    }

}

static void
sse2_composite_over_pixbuf_0565 (pixman_implementation_t *imp,
                                 pixman_op_t              op,
                                 pixman_image_t *         src_image,
                                 pixman_image_t *         mask_image,
                                 pixman_image_t *         dst_image,
                                 int32_t                  src_x,
                                 int32_t                  src_y,
                                 int32_t                  mask_x,
                                 int32_t                  mask_y,
                                 int32_t                  dest_x,
                                 int32_t                  dest_y,
                                 int32_t                  width,
                                 int32_t                  height)
{
    uint16_t    *dst_line, *dst, d;
    uint32_t    *src_line, *src, s;
    int dst_stride, src_stride;
    int32_t w;
    uint32_t opaque, zero;

    __m128i ms;
    __m128i xmm_src, xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst, xmm_dst0, xmm_dst1, xmm_dst2, xmm_dst3;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint16_t, dst_stride, dst_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	src_image, src_x, src_y, uint32_t, src_stride, src_line, 1);

    while (height--)
    {
	dst = dst_line;
	dst_line += dst_stride;
	src = src_line;
	src_line += src_stride;
	w = width;

	while (w && (unsigned long)dst & 15)
	{
	    s = *src++;
	    d = *dst;

	    ms = unpack_32_1x128 (s);

	    *dst++ = pack_565_32_16 (
		pack_1x128_32 (
		    over_rev_non_pre_1x128 (ms, expand565_16_1x128 (d))));
	    w--;
	}

	while (w >= 8)
	{
	    /* First round */
	    xmm_src = load_128_unaligned ((__m128i*)src);
	    xmm_dst = load_128_aligned  ((__m128i*)dst);

	    opaque = is_opaque (xmm_src);
	    zero = is_zero (xmm_src);

	    unpack_565_128_4x128 (xmm_dst,
				  &xmm_dst0, &xmm_dst1, &xmm_dst2, &xmm_dst3);
	    unpack_128_2x128 (xmm_src, &xmm_src_lo, &xmm_src_hi);

	    /* preload next round*/
	    xmm_src = load_128_unaligned ((__m128i*)(src + 4));

	    if (opaque)
	    {
		invert_colors_2x128 (xmm_src_lo, xmm_src_hi,
				     &xmm_dst0, &xmm_dst1);
	    }
	    else if (!zero)
	    {
		over_rev_non_pre_2x128 (xmm_src_lo, xmm_src_hi,
					&xmm_dst0, &xmm_dst1);
	    }

	    /* Second round */
	    opaque = is_opaque (xmm_src);
	    zero = is_zero (xmm_src);

	    unpack_128_2x128 (xmm_src, &xmm_src_lo, &xmm_src_hi);

	    if (opaque)
	    {
		invert_colors_2x128 (xmm_src_lo, xmm_src_hi,
				     &xmm_dst2, &xmm_dst3);
	    }
	    else if (!zero)
	    {
		over_rev_non_pre_2x128 (xmm_src_lo, xmm_src_hi,
					&xmm_dst2, &xmm_dst3);
	    }

	    save_128_aligned (
		(__m128i*)dst, pack_565_4x128_128 (
		    &xmm_dst0, &xmm_dst1, &xmm_dst2, &xmm_dst3));

	    w -= 8;
	    src += 8;
	    dst += 8;
	}

	while (w)
	{
	    s = *src++;
	    d = *dst;

	    ms = unpack_32_1x128 (s);

	    *dst++ = pack_565_32_16 (
		pack_1x128_32 (
		    over_rev_non_pre_1x128 (ms, expand565_16_1x128 (d))));
	    w--;
	}
    }

}

static void
sse2_composite_over_pixbuf_8888 (pixman_implementation_t *imp,
                                 pixman_op_t              op,
                                 pixman_image_t *         src_image,
                                 pixman_image_t *         mask_image,
                                 pixman_image_t *         dst_image,
                                 int32_t                  src_x,
                                 int32_t                  src_y,
                                 int32_t                  mask_x,
                                 int32_t                  mask_y,
                                 int32_t                  dest_x,
                                 int32_t                  dest_y,
                                 int32_t                  width,
                                 int32_t                  height)
{
    uint32_t    *dst_line, *dst, d;
    uint32_t    *src_line, *src, s;
    int dst_stride, src_stride;
    int32_t w;
    uint32_t opaque, zero;

    __m128i xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst_lo, xmm_dst_hi;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint32_t, dst_stride, dst_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	src_image, src_x, src_y, uint32_t, src_stride, src_line, 1);

    while (height--)
    {
	dst = dst_line;
	dst_line += dst_stride;
	src = src_line;
	src_line += src_stride;
	w = width;

	while (w && (unsigned long)dst & 15)
	{
	    s = *src++;
	    d = *dst;

	    *dst++ = pack_1x128_32 (
		over_rev_non_pre_1x128 (
		    unpack_32_1x128 (s), unpack_32_1x128 (d)));

	    w--;
	}

	while (w >= 4)
	{
	    xmm_src_hi = load_128_unaligned ((__m128i*)src);

	    opaque = is_opaque (xmm_src_hi);
	    zero = is_zero (xmm_src_hi);

	    unpack_128_2x128 (xmm_src_hi, &xmm_src_lo, &xmm_src_hi);

	    if (opaque)
	    {
		invert_colors_2x128 (xmm_src_lo, xmm_src_hi,
				     &xmm_dst_lo, &xmm_dst_hi);

		save_128_aligned (
		    (__m128i*)dst, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));
	    }
	    else if (!zero)
	    {
		xmm_dst_hi = load_128_aligned  ((__m128i*)dst);

		unpack_128_2x128 (xmm_dst_hi, &xmm_dst_lo, &xmm_dst_hi);

		over_rev_non_pre_2x128 (xmm_src_lo, xmm_src_hi,
					&xmm_dst_lo, &xmm_dst_hi);

		save_128_aligned (
		    (__m128i*)dst, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));
	    }

	    w -= 4;
	    dst += 4;
	    src += 4;
	}

	while (w)
	{
	    s = *src++;
	    d = *dst;

	    *dst++ = pack_1x128_32 (
		over_rev_non_pre_1x128 (
		    unpack_32_1x128 (s), unpack_32_1x128 (d)));

	    w--;
	}
    }

}

static void
sse2_composite_over_n_8888_0565_ca (pixman_implementation_t *imp,
                                    pixman_op_t              op,
                                    pixman_image_t *         src_image,
                                    pixman_image_t *         mask_image,
                                    pixman_image_t *         dst_image,
                                    int32_t                  src_x,
                                    int32_t                  src_y,
                                    int32_t                  mask_x,
                                    int32_t                  mask_y,
                                    int32_t                  dest_x,
                                    int32_t                  dest_y,
                                    int32_t                  width,
                                    int32_t                  height)
{
    uint32_t src;
    uint16_t    *dst_line, *dst, d;
    uint32_t    *mask_line, *mask, m;
    int dst_stride, mask_stride;
    int w;
    uint32_t pack_cmp;

    __m128i xmm_src, xmm_alpha;
    __m128i xmm_mask, xmm_mask_lo, xmm_mask_hi;
    __m128i xmm_dst, xmm_dst0, xmm_dst1, xmm_dst2, xmm_dst3;

    __m128i mmx_src, mmx_alpha, mmx_mask, mmx_dest;

    src = _pixman_image_get_solid (imp, src_image, dst_image->bits.format);

    if (src == 0)
	return;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint16_t, dst_stride, dst_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	mask_image, mask_x, mask_y, uint32_t, mask_stride, mask_line, 1);

    xmm_src = expand_pixel_32_1x128 (src);
    xmm_alpha = expand_alpha_1x128 (xmm_src);
    mmx_src = xmm_src;
    mmx_alpha = xmm_alpha;

    while (height--)
    {
	w = width;
	mask = mask_line;
	dst = dst_line;
	mask_line += mask_stride;
	dst_line += dst_stride;

	while (w && ((unsigned long)dst & 15))
	{
	    m = *(uint32_t *) mask;

	    if (m)
	    {
		d = *dst;
		mmx_mask = unpack_32_1x128 (m);
		mmx_dest = expand565_16_1x128 (d);

		*dst = pack_565_32_16 (
		    pack_1x128_32 (
			in_over_1x128 (
			    &mmx_src, &mmx_alpha, &mmx_mask, &mmx_dest)));
	    }

	    w--;
	    dst++;
	    mask++;
	}

	while (w >= 8)
	{
	    /* First round */
	    xmm_mask = load_128_unaligned ((__m128i*)mask);
	    xmm_dst = load_128_aligned ((__m128i*)dst);

	    pack_cmp = _mm_movemask_epi8 (
		_mm_cmpeq_epi32 (xmm_mask, _mm_setzero_si128 ()));

	    unpack_565_128_4x128 (xmm_dst,
				  &xmm_dst0, &xmm_dst1, &xmm_dst2, &xmm_dst3);
	    unpack_128_2x128 (xmm_mask, &xmm_mask_lo, &xmm_mask_hi);

	    /* preload next round */
	    xmm_mask = load_128_unaligned ((__m128i*)(mask + 4));

	    /* preload next round */
	    if (pack_cmp != 0xffff)
	    {
		in_over_2x128 (&xmm_src, &xmm_src,
			       &xmm_alpha, &xmm_alpha,
			       &xmm_mask_lo, &xmm_mask_hi,
			       &xmm_dst0, &xmm_dst1);
	    }

	    /* Second round */
	    pack_cmp = _mm_movemask_epi8 (
		_mm_cmpeq_epi32 (xmm_mask, _mm_setzero_si128 ()));

	    unpack_128_2x128 (xmm_mask, &xmm_mask_lo, &xmm_mask_hi);

	    if (pack_cmp != 0xffff)
	    {
		in_over_2x128 (&xmm_src, &xmm_src,
			       &xmm_alpha, &xmm_alpha,
			       &xmm_mask_lo, &xmm_mask_hi,
			       &xmm_dst2, &xmm_dst3);
	    }

	    save_128_aligned (
		(__m128i*)dst, pack_565_4x128_128 (
		    &xmm_dst0, &xmm_dst1, &xmm_dst2, &xmm_dst3));

	    w -= 8;
	    dst += 8;
	    mask += 8;
	}

	while (w)
	{
	    m = *(uint32_t *) mask;

	    if (m)
	    {
		d = *dst;
		mmx_mask = unpack_32_1x128 (m);
		mmx_dest = expand565_16_1x128 (d);

		*dst = pack_565_32_16 (
		    pack_1x128_32 (
			in_over_1x128 (
			    &mmx_src, &mmx_alpha, &mmx_mask, &mmx_dest)));
	    }

	    w--;
	    dst++;
	    mask++;
	}
    }

}

static void
sse2_composite_in_n_8_8 (pixman_implementation_t *imp,
                         pixman_op_t              op,
                         pixman_image_t *         src_image,
                         pixman_image_t *         mask_image,
                         pixman_image_t *         dst_image,
                         int32_t                  src_x,
                         int32_t                  src_y,
                         int32_t                  mask_x,
                         int32_t                  mask_y,
                         int32_t                  dest_x,
                         int32_t                  dest_y,
                         int32_t                  width,
                         int32_t                  height)
{
    uint8_t     *dst_line, *dst;
    uint8_t     *mask_line, *mask;
    int dst_stride, mask_stride;
    uint32_t d, m;
    uint32_t src;
    uint8_t sa;
    int32_t w;

    __m128i xmm_alpha;
    __m128i xmm_mask, xmm_mask_lo, xmm_mask_hi;
    __m128i xmm_dst, xmm_dst_lo, xmm_dst_hi;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint8_t, dst_stride, dst_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	mask_image, mask_x, mask_y, uint8_t, mask_stride, mask_line, 1);

    src = _pixman_image_get_solid (imp, src_image, dst_image->bits.format);

    sa = src >> 24;

    xmm_alpha = expand_alpha_1x128 (expand_pixel_32_1x128 (src));

    while (height--)
    {
	dst = dst_line;
	dst_line += dst_stride;
	mask = mask_line;
	mask_line += mask_stride;
	w = width;

	while (w && ((unsigned long)dst & 15))
	{
	    m = (uint32_t) *mask++;
	    d = (uint32_t) *dst;

	    *dst++ = (uint8_t) pack_1x128_32 (
		pix_multiply_1x128 (
		    pix_multiply_1x128 (xmm_alpha,
				       unpack_32_1x128 (m)),
		    unpack_32_1x128 (d)));
	    w--;
	}

	while (w >= 16)
	{
	    xmm_mask = load_128_unaligned ((__m128i*)mask);
	    xmm_dst = load_128_aligned ((__m128i*)dst);

	    unpack_128_2x128 (xmm_mask, &xmm_mask_lo, &xmm_mask_hi);
	    unpack_128_2x128 (xmm_dst, &xmm_dst_lo, &xmm_dst_hi);

	    pix_multiply_2x128 (&xmm_alpha, &xmm_alpha,
				&xmm_mask_lo, &xmm_mask_hi,
				&xmm_mask_lo, &xmm_mask_hi);

	    pix_multiply_2x128 (&xmm_mask_lo, &xmm_mask_hi,
				&xmm_dst_lo, &xmm_dst_hi,
				&xmm_dst_lo, &xmm_dst_hi);

	    save_128_aligned (
		(__m128i*)dst, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	    mask += 16;
	    dst += 16;
	    w -= 16;
	}

	while (w)
	{
	    m = (uint32_t) *mask++;
	    d = (uint32_t) *dst;

	    *dst++ = (uint8_t) pack_1x128_32 (
		pix_multiply_1x128 (
		    pix_multiply_1x128 (
			xmm_alpha, unpack_32_1x128 (m)),
		    unpack_32_1x128 (d)));
	    w--;
	}
    }

}

static void
sse2_composite_in_n_8 (pixman_implementation_t *imp,
		       pixman_op_t              op,
		       pixman_image_t *         src_image,
		       pixman_image_t *         mask_image,
		       pixman_image_t *         dst_image,
		       int32_t                  src_x,
		       int32_t                  src_y,
		       int32_t                  mask_x,
		       int32_t                  mask_y,
		       int32_t                  dest_x,
		       int32_t                  dest_y,
		       int32_t                  width,
		       int32_t                  height)
{
    uint8_t     *dst_line, *dst;
    int dst_stride;
    uint32_t d;
    uint32_t src;
    int32_t w;

    __m128i xmm_alpha;
    __m128i xmm_dst, xmm_dst_lo, xmm_dst_hi;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint8_t, dst_stride, dst_line, 1);

    src = _pixman_image_get_solid (imp, src_image, dst_image->bits.format);

    xmm_alpha = expand_alpha_1x128 (expand_pixel_32_1x128 (src));

    src = src >> 24;

    if (src == 0xff)
	return;

    if (src == 0x00)
    {
	pixman_fill (dst_image->bits.bits, dst_image->bits.rowstride,
		     8, dest_x, dest_y, width, height, src);

	return;
    }

    while (height--)
    {
	dst = dst_line;
	dst_line += dst_stride;
	w = width;

	while (w && ((unsigned long)dst & 15))
	{
	    d = (uint32_t) *dst;

	    *dst++ = (uint8_t) pack_1x128_32 (
		pix_multiply_1x128 (
		    xmm_alpha,
		    unpack_32_1x128 (d)));
	    w--;
	}

	while (w >= 16)
	{
	    xmm_dst = load_128_aligned ((__m128i*)dst);

	    unpack_128_2x128 (xmm_dst, &xmm_dst_lo, &xmm_dst_hi);
	    
	    pix_multiply_2x128 (&xmm_alpha, &xmm_alpha,
				&xmm_dst_lo, &xmm_dst_hi,
				&xmm_dst_lo, &xmm_dst_hi);

	    save_128_aligned (
		(__m128i*)dst, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	    dst += 16;
	    w -= 16;
	}

	while (w)
	{
	    d = (uint32_t) *dst;

	    *dst++ = (uint8_t) pack_1x128_32 (
		pix_multiply_1x128 (
		    xmm_alpha,
		    unpack_32_1x128 (d)));
	    w--;
	}
    }

}

static void
sse2_composite_in_8_8 (pixman_implementation_t *imp,
                       pixman_op_t              op,
                       pixman_image_t *         src_image,
                       pixman_image_t *         mask_image,
                       pixman_image_t *         dst_image,
                       int32_t                  src_x,
                       int32_t                  src_y,
                       int32_t                  mask_x,
                       int32_t                  mask_y,
                       int32_t                  dest_x,
                       int32_t                  dest_y,
                       int32_t                  width,
                       int32_t                  height)
{
    uint8_t     *dst_line, *dst;
    uint8_t     *src_line, *src;
    int src_stride, dst_stride;
    int32_t w;
    uint32_t s, d;

    __m128i xmm_src, xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst, xmm_dst_lo, xmm_dst_hi;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint8_t, dst_stride, dst_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	src_image, src_x, src_y, uint8_t, src_stride, src_line, 1);

    while (height--)
    {
	dst = dst_line;
	dst_line += dst_stride;
	src = src_line;
	src_line += src_stride;
	w = width;

	while (w && ((unsigned long)dst & 15))
	{
	    s = (uint32_t) *src++;
	    d = (uint32_t) *dst;

	    *dst++ = (uint8_t) pack_1x128_32 (
		pix_multiply_1x128 (
		    unpack_32_1x128 (s), unpack_32_1x128 (d)));
	    w--;
	}

	while (w >= 16)
	{
	    xmm_src = load_128_unaligned ((__m128i*)src);
	    xmm_dst = load_128_aligned ((__m128i*)dst);

	    unpack_128_2x128 (xmm_src, &xmm_src_lo, &xmm_src_hi);
	    unpack_128_2x128 (xmm_dst, &xmm_dst_lo, &xmm_dst_hi);

	    pix_multiply_2x128 (&xmm_src_lo, &xmm_src_hi,
				&xmm_dst_lo, &xmm_dst_hi,
				&xmm_dst_lo, &xmm_dst_hi);

	    save_128_aligned (
		(__m128i*)dst, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	    src += 16;
	    dst += 16;
	    w -= 16;
	}

	while (w)
	{
	    s = (uint32_t) *src++;
	    d = (uint32_t) *dst;

	    *dst++ = (uint8_t) pack_1x128_32 (
		pix_multiply_1x128 (unpack_32_1x128 (s), unpack_32_1x128 (d)));
	    w--;
	}
    }

}

static void
sse2_composite_add_n_8_8 (pixman_implementation_t *imp,
			  pixman_op_t              op,
			  pixman_image_t *         src_image,
			  pixman_image_t *         mask_image,
			  pixman_image_t *         dst_image,
			  int32_t                  src_x,
			  int32_t                  src_y,
			  int32_t                  mask_x,
			  int32_t                  mask_y,
			  int32_t                  dest_x,
			  int32_t                  dest_y,
			  int32_t                  width,
			  int32_t                  height)
{
    uint8_t     *dst_line, *dst;
    uint8_t     *mask_line, *mask;
    int dst_stride, mask_stride;
    int32_t w;
    uint32_t src;
    uint8_t sa;
    uint32_t m, d;

    __m128i xmm_alpha;
    __m128i xmm_mask, xmm_mask_lo, xmm_mask_hi;
    __m128i xmm_dst, xmm_dst_lo, xmm_dst_hi;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint8_t, dst_stride, dst_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	mask_image, mask_x, mask_y, uint8_t, mask_stride, mask_line, 1);

    src = _pixman_image_get_solid (imp, src_image, dst_image->bits.format);

    sa = src >> 24;

    xmm_alpha = expand_alpha_1x128 (expand_pixel_32_1x128 (src));

    while (height--)
    {
	dst = dst_line;
	dst_line += dst_stride;
	mask = mask_line;
	mask_line += mask_stride;
	w = width;

	while (w && ((unsigned long)dst & 15))
	{
	    m = (uint32_t) *mask++;
	    d = (uint32_t) *dst;

	    *dst++ = (uint8_t) pack_1x128_32 (
		_mm_adds_epu16 (
		    pix_multiply_1x128 (
			xmm_alpha, unpack_32_1x128 (m)),
		    unpack_32_1x128 (d)));
	    w--;
	}

	while (w >= 16)
	{
	    xmm_mask = load_128_unaligned ((__m128i*)mask);
	    xmm_dst = load_128_aligned ((__m128i*)dst);

	    unpack_128_2x128 (xmm_mask, &xmm_mask_lo, &xmm_mask_hi);
	    unpack_128_2x128 (xmm_dst, &xmm_dst_lo, &xmm_dst_hi);

	    pix_multiply_2x128 (&xmm_alpha, &xmm_alpha,
				&xmm_mask_lo, &xmm_mask_hi,
				&xmm_mask_lo, &xmm_mask_hi);

	    xmm_dst_lo = _mm_adds_epu16 (xmm_mask_lo, xmm_dst_lo);
	    xmm_dst_hi = _mm_adds_epu16 (xmm_mask_hi, xmm_dst_hi);

	    save_128_aligned (
		(__m128i*)dst, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));

	    mask += 16;
	    dst += 16;
	    w -= 16;
	}

	while (w)
	{
	    m = (uint32_t) *mask++;
	    d = (uint32_t) *dst;

	    *dst++ = (uint8_t) pack_1x128_32 (
		_mm_adds_epu16 (
		    pix_multiply_1x128 (
			xmm_alpha, unpack_32_1x128 (m)),
		    unpack_32_1x128 (d)));

	    w--;
	}
    }

}

static void
sse2_composite_add_n_8 (pixman_implementation_t *imp,
			pixman_op_t              op,
			pixman_image_t *         src_image,
			pixman_image_t *         mask_image,
			pixman_image_t *         dst_image,
			int32_t                  src_x,
			int32_t                  src_y,
			int32_t                  mask_x,
			int32_t                  mask_y,
			int32_t                  dest_x,
			int32_t                  dest_y,
			int32_t                  width,
			int32_t                  height)
{
    uint8_t     *dst_line, *dst;
    int dst_stride;
    int32_t w;
    uint32_t src;

    __m128i xmm_src;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint8_t, dst_stride, dst_line, 1);

    src = _pixman_image_get_solid (imp, src_image, dst_image->bits.format);

    src >>= 24;

    if (src == 0x00)
	return;

    if (src == 0xff)
    {
	pixman_fill (dst_image->bits.bits, dst_image->bits.rowstride,
		     8, dest_x, dest_y, width, height, 0xff);

	return;
    }

    src = (src << 24) | (src << 16) | (src << 8) | src;
    xmm_src = _mm_set_epi32 (src, src, src, src);

    while (height--)
    {
	dst = dst_line;
	dst_line += dst_stride;
	w = width;

	while (w && ((unsigned long)dst & 15))
	{
	    *dst = (uint8_t)_mm_cvtsi128_si32 (
		_mm_adds_epu8 (
		    xmm_src,
		    _mm_cvtsi32_si128 (*dst)));

	    w--;
	    dst++;
	}

	while (w >= 16)
	{
	    save_128_aligned (
		(__m128i*)dst, _mm_adds_epu8 (xmm_src, load_128_aligned  ((__m128i*)dst)));

	    dst += 16;
	    w -= 16;
	}

	while (w)
	{
	    *dst = (uint8_t)_mm_cvtsi128_si32 (
		_mm_adds_epu8 (
		    xmm_src,
		    _mm_cvtsi32_si128 (*dst)));

	    w--;
	    dst++;
	}
    }

}

static void
sse2_composite_add_8_8 (pixman_implementation_t *imp,
			pixman_op_t              op,
			pixman_image_t *         src_image,
			pixman_image_t *         mask_image,
			pixman_image_t *         dst_image,
			int32_t                  src_x,
			int32_t                  src_y,
			int32_t                  mask_x,
			int32_t                  mask_y,
			int32_t                  dest_x,
			int32_t                  dest_y,
			int32_t                  width,
			int32_t                  height)
{
    uint8_t     *dst_line, *dst;
    uint8_t     *src_line, *src;
    int dst_stride, src_stride;
    int32_t w;
    uint16_t t;

    PIXMAN_IMAGE_GET_LINE (
	src_image, src_x, src_y, uint8_t, src_stride, src_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint8_t, dst_stride, dst_line, 1);

    while (height--)
    {
	dst = dst_line;
	src = src_line;

	dst_line += dst_stride;
	src_line += src_stride;
	w = width;

	/* Small head */
	while (w && (unsigned long)dst & 3)
	{
	    t = (*dst) + (*src++);
	    *dst++ = t | (0 - (t >> 8));
	    w--;
	}

	sse2_combine_add_u (imp, op,
			    (uint32_t*)dst, (uint32_t*)src, NULL, w >> 2);

	/* Small tail */
	dst += w & 0xfffc;
	src += w & 0xfffc;

	w &= 3;

	while (w)
	{
	    t = (*dst) + (*src++);
	    *dst++ = t | (0 - (t >> 8));
	    w--;
	}
    }

}

static void
sse2_composite_add_8888_8888 (pixman_implementation_t *imp,
                              pixman_op_t              op,
                              pixman_image_t *         src_image,
                              pixman_image_t *         mask_image,
                              pixman_image_t *         dst_image,
                              int32_t                  src_x,
                              int32_t                  src_y,
                              int32_t                  mask_x,
                              int32_t                  mask_y,
                              int32_t                  dest_x,
                              int32_t                  dest_y,
                              int32_t                  width,
                              int32_t                  height)
{
    uint32_t    *dst_line, *dst;
    uint32_t    *src_line, *src;
    int dst_stride, src_stride;

    PIXMAN_IMAGE_GET_LINE (
	src_image, src_x, src_y, uint32_t, src_stride, src_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint32_t, dst_stride, dst_line, 1);

    while (height--)
    {
	dst = dst_line;
	dst_line += dst_stride;
	src = src_line;
	src_line += src_stride;

	sse2_combine_add_u (imp, op, dst, src, NULL, width);
    }

}

static pixman_bool_t
pixman_blt_sse2 (uint32_t *src_bits,
                 uint32_t *dst_bits,
                 int       src_stride,
                 int       dst_stride,
                 int       src_bpp,
                 int       dst_bpp,
                 int       src_x,
                 int       src_y,
                 int       dst_x,
                 int       dst_y,
                 int       width,
                 int       height)
{
    uint8_t *   src_bytes;
    uint8_t *   dst_bytes;
    int byte_width;

    if (src_bpp != dst_bpp)
	return FALSE;

    if (src_bpp == 16)
    {
	src_stride = src_stride * (int) sizeof (uint32_t) / 2;
	dst_stride = dst_stride * (int) sizeof (uint32_t) / 2;
	src_bytes =(uint8_t *)(((uint16_t *)src_bits) + src_stride * (src_y) + (src_x));
	dst_bytes = (uint8_t *)(((uint16_t *)dst_bits) + dst_stride * (dst_y) + (dst_x));
	byte_width = 2 * width;
	src_stride *= 2;
	dst_stride *= 2;
    }
    else if (src_bpp == 32)
    {
	src_stride = src_stride * (int) sizeof (uint32_t) / 4;
	dst_stride = dst_stride * (int) sizeof (uint32_t) / 4;
	src_bytes = (uint8_t *)(((uint32_t *)src_bits) + src_stride * (src_y) + (src_x));
	dst_bytes = (uint8_t *)(((uint32_t *)dst_bits) + dst_stride * (dst_y) + (dst_x));
	byte_width = 4 * width;
	src_stride *= 4;
	dst_stride *= 4;
    }
    else
    {
	return FALSE;
    }

    while (height--)
    {
	int w;
	uint8_t *s = src_bytes;
	uint8_t *d = dst_bytes;
	src_bytes += src_stride;
	dst_bytes += dst_stride;
	w = byte_width;

	while (w >= 2 && ((unsigned long)d & 3))
	{
	    *(uint16_t *)d = *(uint16_t *)s;
	    w -= 2;
	    s += 2;
	    d += 2;
	}

	while (w >= 4 && ((unsigned long)d & 15))
	{
	    *(uint32_t *)d = *(uint32_t *)s;

	    w -= 4;
	    s += 4;
	    d += 4;
	}

	while (w >= 64)
	{
	    __m128i xmm0, xmm1, xmm2, xmm3;

	    xmm0 = load_128_unaligned ((__m128i*)(s));
	    xmm1 = load_128_unaligned ((__m128i*)(s + 16));
	    xmm2 = load_128_unaligned ((__m128i*)(s + 32));
	    xmm3 = load_128_unaligned ((__m128i*)(s + 48));

	    save_128_aligned ((__m128i*)(d),    xmm0);
	    save_128_aligned ((__m128i*)(d + 16), xmm1);
	    save_128_aligned ((__m128i*)(d + 32), xmm2);
	    save_128_aligned ((__m128i*)(d + 48), xmm3);

	    s += 64;
	    d += 64;
	    w -= 64;
	}

	while (w >= 16)
	{
	    save_128_aligned ((__m128i*)d, load_128_unaligned ((__m128i*)s) );

	    w -= 16;
	    d += 16;
	    s += 16;
	}

	while (w >= 4)
	{
	    *(uint32_t *)d = *(uint32_t *)s;

	    w -= 4;
	    s += 4;
	    d += 4;
	}

	if (w >= 2)
	{
	    *(uint16_t *)d = *(uint16_t *)s;
	    w -= 2;
	    s += 2;
	    d += 2;
	}
    }


    return TRUE;
}

static void
sse2_composite_copy_area (pixman_implementation_t *imp,
                          pixman_op_t              op,
                          pixman_image_t *         src_image,
                          pixman_image_t *         mask_image,
                          pixman_image_t *         dst_image,
                          int32_t                  src_x,
                          int32_t                  src_y,
                          int32_t                  mask_x,
                          int32_t                  mask_y,
                          int32_t                  dest_x,
                          int32_t                  dest_y,
                          int32_t                  width,
                          int32_t                  height)
{
    pixman_blt_sse2 (src_image->bits.bits,
                     dst_image->bits.bits,
                     src_image->bits.rowstride,
                     dst_image->bits.rowstride,
                     PIXMAN_FORMAT_BPP (src_image->bits.format),
                     PIXMAN_FORMAT_BPP (dst_image->bits.format),
                     src_x, src_y, dest_x, dest_y, width, height);
}

static void
sse2_composite_over_x888_8_8888 (pixman_implementation_t *imp,
                                 pixman_op_t              op,
                                 pixman_image_t *         src_image,
                                 pixman_image_t *         mask_image,
                                 pixman_image_t *         dst_image,
                                 int32_t                  src_x,
                                 int32_t                  src_y,
                                 int32_t                  mask_x,
                                 int32_t                  mask_y,
                                 int32_t                  dest_x,
                                 int32_t                  dest_y,
                                 int32_t                  width,
                                 int32_t                  height)
{
    uint32_t    *src, *src_line, s;
    uint32_t    *dst, *dst_line, d;
    uint8_t         *mask, *mask_line;
    uint32_t m;
    int src_stride, mask_stride, dst_stride;
    int32_t w;
    __m128i ms;

    __m128i xmm_src, xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst, xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_mask, xmm_mask_lo, xmm_mask_hi;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint32_t, dst_stride, dst_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	mask_image, mask_x, mask_y, uint8_t, mask_stride, mask_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	src_image, src_x, src_y, uint32_t, src_stride, src_line, 1);

    while (height--)
    {
        src = src_line;
        src_line += src_stride;
        dst = dst_line;
        dst_line += dst_stride;
        mask = mask_line;
        mask_line += mask_stride;

        w = width;

        while (w && (unsigned long)dst & 15)
        {
            s = 0xff000000 | *src++;
            m = (uint32_t) *mask++;
            d = *dst;
            ms = unpack_32_1x128 (s);

            if (m != 0xff)
            {
		__m128i ma = expand_alpha_rev_1x128 (unpack_32_1x128 (m));
		__m128i md = unpack_32_1x128 (d);

                ms = in_over_1x128 (&ms, &mask_00ff, &ma, &md);
            }

            *dst++ = pack_1x128_32 (ms);
            w--;
        }

        while (w >= 4)
        {
            m = *(uint32_t*) mask;
            xmm_src = _mm_or_si128 (
		load_128_unaligned ((__m128i*)src), mask_ff000000);

            if (m == 0xffffffff)
            {
                save_128_aligned ((__m128i*)dst, xmm_src);
            }
            else
            {
                xmm_dst = load_128_aligned ((__m128i*)dst);

                xmm_mask = _mm_unpacklo_epi16 (unpack_32_1x128 (m), _mm_setzero_si128());

                unpack_128_2x128 (xmm_src, &xmm_src_lo, &xmm_src_hi);
                unpack_128_2x128 (xmm_mask, &xmm_mask_lo, &xmm_mask_hi);
                unpack_128_2x128 (xmm_dst, &xmm_dst_lo, &xmm_dst_hi);

                expand_alpha_rev_2x128 (
		    xmm_mask_lo, xmm_mask_hi, &xmm_mask_lo, &xmm_mask_hi);

                in_over_2x128 (&xmm_src_lo, &xmm_src_hi,
			       &mask_00ff, &mask_00ff, &xmm_mask_lo, &xmm_mask_hi,
			       &xmm_dst_lo, &xmm_dst_hi);

                save_128_aligned ((__m128i*)dst, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));
            }

            src += 4;
            dst += 4;
            mask += 4;
            w -= 4;
        }

        while (w)
        {
            m = (uint32_t) *mask++;

            if (m)
            {
                s = 0xff000000 | *src;

                if (m == 0xff)
                {
                    *dst = s;
                }
                else
                {
		    __m128i ma, md, ms;

                    d = *dst;

		    ma = expand_alpha_rev_1x128 (unpack_32_1x128 (m));
		    md = unpack_32_1x128 (d);
		    ms = unpack_32_1x128 (s);

                    *dst = pack_1x128_32 (in_over_1x128 (&ms, &mask_00ff, &ma, &md));
                }

            }

            src++;
            dst++;
            w--;
        }
    }

}

static void
sse2_composite_over_8888_8_8888 (pixman_implementation_t *imp,
                                 pixman_op_t              op,
                                 pixman_image_t *         src_image,
                                 pixman_image_t *         mask_image,
                                 pixman_image_t *         dst_image,
                                 int32_t                  src_x,
                                 int32_t                  src_y,
                                 int32_t                  mask_x,
                                 int32_t                  mask_y,
                                 int32_t                  dest_x,
                                 int32_t                  dest_y,
                                 int32_t                  width,
                                 int32_t                  height)
{
    uint32_t    *src, *src_line, s;
    uint32_t    *dst, *dst_line, d;
    uint8_t         *mask, *mask_line;
    uint32_t m;
    int src_stride, mask_stride, dst_stride;
    int32_t w;

    __m128i xmm_src, xmm_src_lo, xmm_src_hi, xmm_srca_lo, xmm_srca_hi;
    __m128i xmm_dst, xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_mask, xmm_mask_lo, xmm_mask_hi;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint32_t, dst_stride, dst_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	mask_image, mask_x, mask_y, uint8_t, mask_stride, mask_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	src_image, src_x, src_y, uint32_t, src_stride, src_line, 1);

    while (height--)
    {
        src = src_line;
        src_line += src_stride;
        dst = dst_line;
        dst_line += dst_stride;
        mask = mask_line;
        mask_line += mask_stride;

        w = width;

        while (w && (unsigned long)dst & 15)
        {
	    uint32_t sa;

            s = *src++;
            m = (uint32_t) *mask++;
            d = *dst;

	    sa = s >> 24;

	    if (m)
	    {
		if (sa == 0xff && m == 0xff)
		{
		    *dst = s;
		}
		else
		{
		    __m128i ms, md, ma, msa;

		    ma = expand_alpha_rev_1x128 (load_32_1x128 (m));
		    ms = unpack_32_1x128 (s);
		    md = unpack_32_1x128 (d);

		    msa = expand_alpha_rev_1x128 (load_32_1x128 (sa));

		    *dst = pack_1x128_32 (in_over_1x128 (&ms, &msa, &ma, &md));
		}
	    }

	    dst++;
            w--;
        }

        while (w >= 4)
        {
            m = *(uint32_t *) mask;

	    if (m)
	    {
		xmm_src = load_128_unaligned ((__m128i*)src);

		if (m == 0xffffffff && is_opaque (xmm_src))
		{
		    save_128_aligned ((__m128i *)dst, xmm_src);
		}
		else
		{
		    xmm_dst = load_128_aligned ((__m128i *)dst);

		    xmm_mask = _mm_unpacklo_epi16 (unpack_32_1x128 (m), _mm_setzero_si128());

		    unpack_128_2x128 (xmm_src, &xmm_src_lo, &xmm_src_hi);
		    unpack_128_2x128 (xmm_mask, &xmm_mask_lo, &xmm_mask_hi);
		    unpack_128_2x128 (xmm_dst, &xmm_dst_lo, &xmm_dst_hi);

		    expand_alpha_2x128 (xmm_src_lo, xmm_src_hi, &xmm_srca_lo, &xmm_srca_hi);
		    expand_alpha_rev_2x128 (xmm_mask_lo, xmm_mask_hi, &xmm_mask_lo, &xmm_mask_hi);

		    in_over_2x128 (&xmm_src_lo, &xmm_src_hi, &xmm_srca_lo, &xmm_srca_hi,
				   &xmm_mask_lo, &xmm_mask_hi, &xmm_dst_lo, &xmm_dst_hi);

		    save_128_aligned ((__m128i*)dst, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));
		}
	    }

            src += 4;
            dst += 4;
            mask += 4;
            w -= 4;
        }

        while (w)
        {
	    uint32_t sa;

            s = *src++;
            m = (uint32_t) *mask++;
            d = *dst;

	    sa = s >> 24;

	    if (m)
	    {
		if (sa == 0xff && m == 0xff)
		{
		    *dst = s;
		}
		else
		{
		    __m128i ms, md, ma, msa;

		    ma = expand_alpha_rev_1x128 (load_32_1x128 (m));
		    ms = unpack_32_1x128 (s);
		    md = unpack_32_1x128 (d);

		    msa = expand_alpha_rev_1x128 (load_32_1x128 (sa));

		    *dst = pack_1x128_32 (in_over_1x128 (&ms, &msa, &ma, &md));
		}
	    }

	    dst++;
            w--;
        }
    }

}

static void
sse2_composite_over_reverse_n_8888 (pixman_implementation_t *imp,
				    pixman_op_t              op,
				    pixman_image_t *         src_image,
				    pixman_image_t *         mask_image,
				    pixman_image_t *         dst_image,
				    int32_t                  src_x,
				    int32_t                  src_y,
				    int32_t                  mask_x,
				    int32_t                  mask_y,
				    int32_t                  dest_x,
				    int32_t                  dest_y,
				    int32_t                  width,
				    int32_t                  height)
{
    uint32_t src;
    uint32_t    *dst_line, *dst;
    __m128i xmm_src;
    __m128i xmm_dst, xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_dsta_hi, xmm_dsta_lo;
    int dst_stride;
    int32_t w;

    src = _pixman_image_get_solid (imp, src_image, dst_image->bits.format);

    if (src == 0)
	return;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint32_t, dst_stride, dst_line, 1);

    xmm_src = expand_pixel_32_1x128 (src);

    while (height--)
    {
	dst = dst_line;

	dst_line += dst_stride;
	w = width;

	while (w && (unsigned long)dst & 15)
	{
	    __m128i vd;

	    vd = unpack_32_1x128 (*dst);

	    *dst = pack_1x128_32 (over_1x128 (vd, expand_alpha_1x128 (vd),
					      xmm_src));
	    w--;
	    dst++;
	}

	while (w >= 4)
	{
	    __m128i tmp_lo, tmp_hi;

	    xmm_dst = load_128_aligned ((__m128i*)dst);

	    unpack_128_2x128 (xmm_dst, &xmm_dst_lo, &xmm_dst_hi);
	    expand_alpha_2x128 (xmm_dst_lo, xmm_dst_hi, &xmm_dsta_lo, &xmm_dsta_hi);

	    tmp_lo = xmm_src;
	    tmp_hi = xmm_src;

	    over_2x128 (&xmm_dst_lo, &xmm_dst_hi,
			&xmm_dsta_lo, &xmm_dsta_hi,
			&tmp_lo, &tmp_hi);

	    save_128_aligned (
		(__m128i*)dst, pack_2x128_128 (tmp_lo, tmp_hi));

	    w -= 4;
	    dst += 4;
	}

	while (w)
	{
	    __m128i vd;

	    vd = unpack_32_1x128 (*dst);

	    *dst = pack_1x128_32 (over_1x128 (vd, expand_alpha_1x128 (vd),
					      xmm_src));
	    w--;
	    dst++;
	}

    }

}

static void
sse2_composite_over_8888_8888_8888 (pixman_implementation_t *imp,
				    pixman_op_t              op,
				    pixman_image_t *         src_image,
				    pixman_image_t *         mask_image,
				    pixman_image_t *         dst_image,
				    int32_t                  src_x,
				    int32_t                  src_y,
				    int32_t                  mask_x,
				    int32_t                  mask_y,
				    int32_t                  dest_x,
				    int32_t                  dest_y,
				    int32_t                  width,
				    int32_t                  height)
{
    uint32_t    *src, *src_line, s;
    uint32_t    *dst, *dst_line, d;
    uint32_t    *mask, *mask_line;
    uint32_t    m;
    int src_stride, mask_stride, dst_stride;
    int32_t w;

    __m128i xmm_src, xmm_src_lo, xmm_src_hi, xmm_srca_lo, xmm_srca_hi;
    __m128i xmm_dst, xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_mask, xmm_mask_lo, xmm_mask_hi;

    PIXMAN_IMAGE_GET_LINE (
	dst_image, dest_x, dest_y, uint32_t, dst_stride, dst_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	mask_image, mask_x, mask_y, uint32_t, mask_stride, mask_line, 1);
    PIXMAN_IMAGE_GET_LINE (
	src_image, src_x, src_y, uint32_t, src_stride, src_line, 1);

    while (height--)
    {
        src = src_line;
        src_line += src_stride;
        dst = dst_line;
        dst_line += dst_stride;
        mask = mask_line;
        mask_line += mask_stride;

        w = width;

        while (w && (unsigned long)dst & 15)
        {
	    uint32_t sa;

            s = *src++;
            m = (*mask++) >> 24;
            d = *dst;

	    sa = s >> 24;

	    if (m)
	    {
		if (sa == 0xff && m == 0xff)
		{
		    *dst = s;
		}
		else
		{
		    __m128i ms, md, ma, msa;

		    ma = expand_alpha_rev_1x128 (load_32_1x128 (m));
		    ms = unpack_32_1x128 (s);
		    md = unpack_32_1x128 (d);

		    msa = expand_alpha_rev_1x128 (load_32_1x128 (sa));

		    *dst = pack_1x128_32 (in_over_1x128 (&ms, &msa, &ma, &md));
		}
	    }

	    dst++;
            w--;
        }

        while (w >= 4)
        {
	    xmm_mask = load_128_unaligned ((__m128i*)mask);

	    if (!is_transparent (xmm_mask))
	    {
		xmm_src = load_128_unaligned ((__m128i*)src);

		if (is_opaque (xmm_mask) && is_opaque (xmm_src))
		{
		    save_128_aligned ((__m128i *)dst, xmm_src);
		}
		else
		{
		    xmm_dst = load_128_aligned ((__m128i *)dst);

		    unpack_128_2x128 (xmm_src, &xmm_src_lo, &xmm_src_hi);
		    unpack_128_2x128 (xmm_mask, &xmm_mask_lo, &xmm_mask_hi);
		    unpack_128_2x128 (xmm_dst, &xmm_dst_lo, &xmm_dst_hi);

		    expand_alpha_2x128 (xmm_src_lo, xmm_src_hi, &xmm_srca_lo, &xmm_srca_hi);
		    expand_alpha_2x128 (xmm_mask_lo, xmm_mask_hi, &xmm_mask_lo, &xmm_mask_hi);

		    in_over_2x128 (&xmm_src_lo, &xmm_src_hi, &xmm_srca_lo, &xmm_srca_hi,
				   &xmm_mask_lo, &xmm_mask_hi, &xmm_dst_lo, &xmm_dst_hi);

		    save_128_aligned ((__m128i*)dst, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));
		}
	    }

            src += 4;
            dst += 4;
            mask += 4;
            w -= 4;
        }

        while (w)
        {
	    uint32_t sa;

            s = *src++;
            m = (*mask++) >> 24;
            d = *dst;

	    sa = s >> 24;

	    if (m)
	    {
		if (sa == 0xff && m == 0xff)
		{
		    *dst = s;
		}
		else
		{
		    __m128i ms, md, ma, msa;

		    ma = expand_alpha_rev_1x128 (load_32_1x128 (m));
		    ms = unpack_32_1x128 (s);
		    md = unpack_32_1x128 (d);

		    msa = expand_alpha_rev_1x128 (load_32_1x128 (sa));

		    *dst = pack_1x128_32 (in_over_1x128 (&ms, &msa, &ma, &md));
		}
	    }

	    dst++;
            w--;
        }
    }

}

/* A variant of 'sse2_combine_over_u' with minor tweaks */
static force_inline void
scaled_nearest_scanline_sse2_8888_8888_OVER (uint32_t*       pd,
                                             const uint32_t* ps,
                                             int32_t         w,
                                             pixman_fixed_t  vx,
                                             pixman_fixed_t  unit_x,
                                             pixman_fixed_t  max_vx,
                                             pixman_bool_t   fully_transparent_src)
{
    uint32_t s, d;
    const uint32_t* pm = NULL;

    __m128i xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_src_lo, xmm_src_hi;
    __m128i xmm_alpha_lo, xmm_alpha_hi;

    if (fully_transparent_src)
	return;

    /* Align dst on a 16-byte boundary */
    while (w && ((unsigned long)pd & 15))
    {
	d = *pd;
	s = combine1 (ps + (vx >> 16), pm);
	vx += unit_x;

	*pd++ = core_combine_over_u_pixel_sse2 (s, d);
	if (pm)
	    pm++;
	w--;
    }

    while (w >= 4)
    {
	__m128i tmp;
	uint32_t tmp1, tmp2, tmp3, tmp4;

	tmp1 = ps[vx >> 16];
	vx += unit_x;
	tmp2 = ps[vx >> 16];
	vx += unit_x;
	tmp3 = ps[vx >> 16];
	vx += unit_x;
	tmp4 = ps[vx >> 16];
	vx += unit_x;

	tmp = _mm_set_epi32 (tmp4, tmp3, tmp2, tmp1);

	xmm_src_hi = combine4 ((__m128i*)&tmp, (__m128i*)pm);

	if (is_opaque (xmm_src_hi))
	{
	    save_128_aligned ((__m128i*)pd, xmm_src_hi);
	}
	elses_ope, 1);

    while (height--)
    {
        src = src_line;
 n_f   {
        src = src_line;
 n_f   {
 , uint8_t, src_rc = src_line;
_rc = src_l  = src_line;
 n_f   {
 , uint8_t, src_rc = src_l2 = ps[vx >> 16];
	vx += unit_x;
	tmp3 6];
	vx += unit_x;
	tmp3 6];
	vx += unigOn_f   {28i*)dst, )&tm	
	tmp3 6];28 (xmm_mA2_1x128 (dc_l2 =esa 6];
	vx += unigOn_f   {28l2 =
				&xmm_dst_lo, &xmm_dst_hi);

	    /rebuolithe 4 u_pix data vx    sa*/	{
	    save_128_aligned ((__m128i*)pi,
			   t, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));  }
  w -= 4;p  d += 4;
	if (pm)
	     d +=--;
    }

    while 5))
    {
	d = *pd;
	s = combine1 (ps + (vx >> 16), pm);
	vx += unit_x;

	*pd++ = core_combine_over_u_pixel_sse2 (s, d);
	if (pm)
	    pm+++;
	w--;
      }FAST_NEAREST_MAINLOOBPPe_sse2_8888_888ce_ov88_OVth,
		     d
scaled_nearest_scanline_sse2_8888_8888_OVth,
		    y, uint32_y, uint32_yC8_OV)}FAST_NEAREST_MAINLOOBPPe_sse2_8888_888none88_OVth,
		     d
scaled_nearest_scanline_sse2_8888_8888_OVth,
		    y, uint32_y, uint32_yNONE)}FAST_NEAREST_MAINLOOBPPe_sse2_8888_888pad88_OVth,
		     d
scaled_nearest_scanline_sse2_8888_8888_OVth,
		    y, uint32_y, uint32_yPAD)
*/
static force_inline void
scaled_nearest_scanline_sse2_888n88_8888_OVER  const uint32 *   mad),
					    st uint32 *         dd),
					    s  const uint32 *   sd),
					 		    int32_t        wd),
					       pixman_fixed__t  d),
					       pixman_fixed__t= uni d),
					       pixman_fixed__t  ma  d),
				        pixman_bool_t  1);
nt_src)
{
    __m128i xmm_ma;;

    __m128i xmm_src, xmm_src_lo, xmm_src_hi;
    __m128i xmm_dst, xmm_dst_lo, xmm_dst_hi;
    __m128i xmm_alpha_lo, xmm_alpha_hi;

    if1);
nt_s || = (*ma s >> )rc == 0)
	return;
	    xmm_mask createmm_mas1628_1 = (*ma s >> )rn;      while (w && (unsigned long)dst & 15)
  {p;
	uint32_ *s = s[  pixman_fixedo_	ui + ()16];
	vx += unit_x;
		if eight--)
 
	uint32   d = *dst;

	    __m128  ms = unpack_32_1x128 (s;

	    __m128m_alp
	  ha = expand_alpha_1x128m (s;

	    __m12x, de 
	  ha8i xmm_ma;;

	    __m128m_alpmm_dmd = unpack_32_1x128 st);

	    *dst = pack_1x128_32),
 (in_over_1x128 (&ms,m_alpha,, dems,m_alpmm_dhi));  (
	dpm++;
	w--;
    }

    while (w >= 4)
    {
	uint32_t tmp1, tmp2, tmp3, tmp4;

	tmp1 = s[  pixman_fixedo_	ui + ()16];
	vx += unit_x;
	tmp1 = s[  pixman_fixedo_	ui + ()16];
	vx += unit_x;
	t3p1 = s[  pixman_fixedo_	ui + ()16];
	vx += unit_x;
	t4p1 = s[  pixman_fixedo_	ui + ()16];
	vx += unit_x;

	xmm_smp = _mm_set_epi32 (tmp4, tmp3, tmp2, tmp1);
es_ope, 1);

    whil)16)
	{
	    xmm_dst = load_128_aligned ((__m128i*)dst);

	    unpack_128_2x128 (xmm_src, &xmm_src_lo, &xmm_src_hi);		    unpack_128_2x128 (xmm_dst, &xmm_dst_lo, &xmm_dst_hi)
		    expand_alpha_2x128 (xmm_src_lo, xmm_src_hi,
			   t 3 6];
	vx += unigOn_f   {28i*)dst, )&t  in_over_2x128 (&xmm_src_lo, &xmm_src_hi,
			  6];
	vx += unigOn_f   {28l2 =
				   (xmm_mask, &xmm_ma =
				_t, src_rc = src_l2 = ps[vx >> 16]  save_128_aligned (
		(__m128i*)dst, pack_2x128_128 (xmm_dst_lo, xmm_dst_hi));  }
  dst += 4
  w +=--;
    }

    while 5))
    {
	uint32_ *s = s[  pixman_fixedo_	ui + ()16];
	vx += unit_x;
		if eight--)
 
	uint32   d = *dst;

	    __m128  ms = unpack_32_1x128 (s;

	    __m128m_alp
a = expand_alpha_1x128m (s;

	    __m12xm_ma ha8i xmm_ma;;

	    __m12x, de md = unpack_32_1x128 st);

	    *dst = pack_1x128_32),
 (in_over_1x128 (&ms,m_alpha,m_mask,, dehi));  }
 	dpm++;
	w--;
       }FAST_NEAREST_MAINLOOB_COMMONPPe_sse2_888n88_888ce_ov88_OVth,
		     d
scaled_nearest_scanline_sse2_888n88_8888_OVth,
		    y, uint32_y, uint32_y, uint32_yC8_OV,rn TR,rn TR)}FAST_NEAREST_MAINLOOB_COMMONPPe_sse2_888n88_888pad88_OVth,
		     d
scaled_nearest_scanline_sse2_888n88_8888_OVth,
		    y, uint32_y, uint32_y, uint32_yPAD,rn TR,rn TR)}FAST_NEAREST_MAINLOOB_COMMONPPe_sse2_888n88_888none88_OVth,
		     d
scaled_nearest_scanline_sse2_888n88_8888_OVth,
		    y, uint32_y, uint32_y, uint32_yNONE,rn TR,rn TR)
*/
stati  con   pixmanarespath32 e_ssanarespaths[] =c)

	       PIXMAOB_8_OVry */
    PIXMASTD_FAST_PATH (8_OV,rt_sol, a8, r5g6b5, d
sse2_composite_oved_n_0565),*/
    PIXMASTD_FAST_PATH (8_OV,rt_sol, a8, b5g6r5, d
sse2_composite_oved_n_0565),*/
    PIXMASTD_FAST_PATH (8_OV,rt_sol, n(fu, a8r8g8b8, d
sse2_composite_oved_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,rt_sol, n(fu, x8r8g8b8, d
sse2_composite_oved_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,rt_sol, n(fu, r5g6b5, d
sse2_composite_oved_0565),*/
    PIXMASTD_FAST_PATH (8_OV,ra8r8g8b8, n(fu, a8r8g8b8, d
sse2_composite_ovennnn_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,ra8r8g8b8, n(fu, x8r8g8b8, d
sse2_composite_ovennnn_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,ra8b8g8r8, n(fu,ra8b8g8r8, d
sse2_composite_ovennnn_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,ra8b8g8r8, n(fu,rx8b8g8r8, d
sse2_composite_ovennnn_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,ra8r8g8b8, n(fu, r5g6b5, d
sse2_composite_ovennnn_0565),*/
    PIXMASTD_FAST_PATH (8_OV,ra8b8g8r8, n(fu, b5g6r5, d
sse2_composite_ovennnn_0565),*/
    PIXMASTD_FAST_PATH (8_OV,rt_sol, a8, a8r8g8b8, d
sse2_composite_oved_n_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,rt_sol, a8, x8r8g8b8, d
sse2_composite_oved_n_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,rt_sol, a8, a8b8g8r8, d
sse2_composite_oved_n_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,rt_sol, a8, x8b8g8r8, d
sse2_composite_oved_n_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,ra8r8g8b8, a8r8g8b8, a8r8g8b8, d
sse2_composite_ovennnnennnn_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,ra8r8g8b8, a8, x8r8g8b8, d
sse2_composite_ovennnn_n_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,ra8r8g8b8, a8, a8r8g8b8, d
sse2_composite_ovennnnen_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,ra8b8g8r8, a8, x8b8g8r8, d
sse2_composite_ovennnnen_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,ra8b8g8r8, a8, a8b8g8r8, d
sse2_composite_ovennnn_n_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,rx8r8g8b8, a8, x8r8g8b8, d
sse2_composite_ovexnnn_n_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,rx8r8g8b8, a8, a8r8g8b8, d
sse2_composite_ovexnnn_n_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,rx8b8g8r8, a8, x8b8g8r8, d
sse2_composite_ovexnnn_n_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,rx8b8g8r8, a8, a8b8g8r8, d
sse2_composite_ovexnnn_n_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,rx8r8g8b8,rt_sol, a8r8g8b8, d
sse2_composite_ovexnnn_d_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,rx8r8g8b8,rt_sol, x8r8g8b8, d
sse2_composite_ovexnnn_d_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,rx8b8g8r8,rt_sol, a8b8g8r8, d
sse2_composite_ovexnnn_d_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,rx8b8g8r8,rt_sol, x8b8g8r8, d
sse2_composite_ovexnnn_d_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,ra8r8g8b8,rt_sol, a8r8g8b8, d
sse2_composite_ove8nnn_d_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,ra8r8g8b8,rt_sol, x8r8g8b8, d
sse2_composite_ovennnn_d_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,ra8b8g8r8,rt_sol, a8b8g8r8, d
sse2_composite_ovennnn_d_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,ra8b8g8r8,rt_sol, x8b8g8r8, d
sse2_composite_ovennnned_nnnn),*/
    PIXMASTD_FAST_PATH_CA (8_OV,rt_sol, a8r8g8b8, a8r8g8b8, d
sse2_composite_ovene2_8888_888ca),*/
    PIXMASTD_FAST_PATH_CA (8_OV,rt_sol, a8r8g8b8, x8r8g8b8, d
sse2_composite_oved_n_8888_888ca),*/
    PIXMASTD_FAST_PATH_CA (8_OV,rt_sol, a8b8g8r8, a8b8g8r8, d
sse2_composite_oved_n_8888_888ca),*/
    PIXMASTD_FAST_PATH_CA (8_OV,rt_sol, a8b8g8r8, x8b8g8r8, d
sse2_composite_oved_n_8888_888ca),*/
    PIXMASTD_FAST_PATH_CA (8_OV,rt_sol, a8r8g8b8, r5g6b5, d
sse2_composite_oved_nnnn_05658ca),*/
    PIXMASTD_FAST_PATH_CA (8_OV,rt_sol, a8b8g8r8, b5g6r5, d
sse2_composite_oved_nnnn_05658ca),*/
    PIXMASTD_FAST_PATH (8_OV,   pbuf,   pbuf, a8r8g8b8, d
sse2_composite_ove  pbuf_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,r  pbuf,   pbuf, x8r8g8b8, d
sse2_composite_ove  pbuf_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,rr  pbuf,rr  pbuf, a8b8g8r8, d
sse2_composite_ove  pbuf_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,rr  pbuf,rr  pbuf, x8b8g8r8, d
sse2_composite_ove  pbuf_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV,r  pbuf,   pbuf, r5g6b5, d
sse2_composite_ove  pbuf_0565),*/
    PIXMASTD_FAST_PATH (8_OV,rr  pbuf,rr  pbuf, b5g6r5, d
sse2_composite_ove  pbuf_0565),*/
    PIXMASTD_FAST_PATH (8_OV,rx8r8g8b8, n(fu, x8r8g8b8, d
sse2_composite_copy_aa),*/
    PIXMASTD_FAST_PATH (8_OV, x8b8g8r8, n(fu,rx8b8g8r8, d
sse2_composite_copy_aa),*/
  

	       PIXMAOB_8_OV_RE_OVSEry */
    PIXMASTD_FAST_PATH (8_OV_RE_OVSE,rt_sol, n(fu, a8r8g8b8, d
sse2_composite_over_revered_nnnn),*/
    PIXMASTD_FAST_PATH (8_OV_RE_OVSE,rt_sol, n(fu,ra8b8g8r8, d
sse2_composite_over_revered_nnnn),*

	       PIXMAOB_ADDry */
    PIXMASTD_FAST_PATH_CA (ADD,rt_sol, a8r8g8b8, a8r8g8b8, d
sse2_composite_aed_n_8888_888ca),*/
    PIXMASTD_FAST_PATH (ADD,ra8, n(fu,ra8, d
sse2_composite_ae888),*/
    PIXMASTD_FAST_PATH (ADD,ra8r8g8b8, n(fu, a8r8g8b8, d
sse2_composite_aennnn_nnnn),*/
    PIXMASTD_FAST_PATH (ADD,ra8b8g8r8, n(fu,ra8b8g8r8, d
sse2_composite_aennnn_nnnn),*/
    PIXMASTD_FAST_PATH (ADD,rt_sol, a8, a8, d
sse2_composite_aede888),*/
    PIXMASTD_FAST_PATH (ADD,rt_sol, n(fu,ra8, d
sse2_composite_aeden),*

	       PIXMAOB_SRCry */
    PIXMASTD_FAST_PATH (SRC,rt_sol, a8, a8r8g8b8, d
sse2_composit= sed_n_nnnn),*/
    PIXMASTD_FAST_PATH (SRC,rt_sol, a8, x8r8g8b8, d
sse2_composit= sed_n_nnnn),*/
    PIXMASTD_FAST_PATH (SRC,rt_sol, a8, a8b8g8r8, d
sse2_composit= sed_n_nnnn),*/
    PIXMASTD_FAST_PATH (SRC,rt_sol, a8, x8b8g8r8, d
sse2_composit= sed_n_nnnn),*/
    PIXMASTD_FAST_PATH (SRC,rx8r8g8b8, n(fu, a8r8g8b8, d
sse2_composit, srcnnn_nnnn),*/
    PIXMASTD_FAST_PATH (SRC,rx8b8g8r8, n(fu,ra8b8g8r8, d
sse2_composit, srcnnn_nnnn),*/
    PIXMASTD_FAST_PATH (SRC,ra8r8g8b8, n(fu, a8r8g8b8, d
sse2_composite_copy_aa),*/
    PIXMASTD_FAST_PATH (SRC,ra8b8g8r8, n(fu,ra8b8g8r8, d
sse2_composite_copy_aa),*/
    PIXMASTD_FAST_PATH (SRC,ra8r8g8b8, n(fu, x8r8g8b8, d
sse2_composite_copy_aa),*/
    PIXMASTD_FAST_PATH (SRC,ra8b8g8r8, n(fu,rx8b8g8r8, d
sse2_composite_copy_aa),*/
    PIXMASTD_FAST_PATH (SRC,rx8r8g8b8, n(fu, x8r8g8b8, d
sse2_composite_copy_aa),*/
    PIXMASTD_FAST_PATH (SRC, x8b8g8r8, n(fu,rx8b8g8r8, d
sse2_composite_copy_aa),*/
    PIXMASTD_FAST_PATH (SRC, r5g6b5, n(fu, r5g6b5, d
sse2_composite_copy_aa),*/
    PIXMASTD_FAST_PATH (SRC, b5g6r5, n(fu, b5g6r5, d
sse2_composite_copy_aa),*

	       PIXMAOB_INry */
    PIXMASTD_FAST_PATH (IN,ra8, n(fu,ra8, d
sse2_compositide888),*/
    PIXMASTD_FAST_PATH (IN,rt_sol, a8,ra8, d
sse2_compositidede888),*/
    PIXMASTD_FAST_PATH (IN,rt_sol, n(fu,ra8, d
sse2_compositidede8),*

	  SIMPLE_NEAREST_FAST_PATH_C8_OV (8_OV,ra8r8g8b8, x8r8g8b8, d
ssennnn_nnnn),

	  SIMPLE_NEAREST_FAST_PATH_C8_OV (8_OV,ra8b8g8r8, x8b8g8r8, d
ssennnn_nnnn),

	  SIMPLE_NEAREST_FAST_PATH_C8_OV (8_OV,ra8r8g8b8, a8r8g8b8, d
ssennnn_nnnn),

	  SIMPLE_NEAREST_FAST_PATH_C8_OV (8_OV,ra8b8g8r8, a8b8g8r8, d
ssennnn_nnnn),

	  SIMPLE_NEAREST_FAST_PATH_NONE (8_OV,ra8r8g8b8, x8r8g8b8, d
ssennnn_nnnn),

	  SIMPLE_NEAREST_FAST_PATH_NONE (8_OV,ra8b8g8r8, x8b8g8r8, d
ssennnn_nnnn),

	  SIMPLE_NEAREST_FAST_PATH_NONE (8_OV,ra8r8g8b8, a8r8g8b8, d
ssennnn_nnnn),

	  SIMPLE_NEAREST_FAST_PATH_NONE (8_OV,ra8b8g8r8, a8b8g8r8, d
ssennnn_nnnn),

	  SIMPLE_NEAREST_FAST_PATH_PAD (8_OV,ra8r8g8b8, x8r8g8b8, d
ssennnn_nnnn),

	  SIMPLE_NEAREST_FAST_PATH_PAD (8_OV,ra8b8g8r8, x8b8g8r8, d
ssennnn_nnnn),

	  SIMPLE_NEAREST_FAST_PATH_PAD (8_OV,ra8r8g8b8, a8r8g8b8, d
ssennnn_nnnn),

	  SIMPLE_NEAREST_FAST_PATH_PAD (8_OV,ra8b8g8r8, a8b8g8r8, d
ssennnn_nnnn),


	  SIMPLE_NEAREST_SOLID_MASK_FAST_PATH (8_OV,ra8r8g8b8, a8r8g8b8, d
ssennnn_d_nnnn),

	  SIMPLE_NEAREST_SOLID_MASK_FAST_PATH (8_OV,ra8b8g8r8, a8b8g8r8, d
ssennnn_d_nnnn),

	  SIMPLE_NEAREST_SOLID_MASK_FAST_PATH (8_OV,ra8r8g8b8, x8r8g8b8, d
ssennnn_d_nnnn),

	  SIMPLE_NEAREST_SOLID_MASK_FAST_PATH (8_OV,ra8b8g8r8, x8b8g8r8, d
ssennnn_d_nnnn),
))
      PIXMAOB_NONE },
};
}

static pixman_bool_d
ssan_b 8 (pixman_implementation_t *imp,
        t uint32 *               *)src_bimp,
        t uint32 *                *dst_bits,
         ui               nt       src_strits,
         ui               nt      *drc_strits,
         ui               nt      (src_bts,
         ui               nt      *drc_bts,
         ui               nt     , srcts,
         ui               nt     , sryts,
         ui               nt      *drcts,
         ui               nt      *dryts,
         ui               nt     = widts,
         ui               nt       height)

	    if   pixman_blt_sse2
            *)src_bim  *dst_bit   src_strit  *drc_strit  (src_bt  *drc_bts              src_x, src_y,dest_x,dest_y, width, heigh)
se
    {
	retur_ (pixman_implementatian_b 8;

	  n_i->delegatage,    *)src_bim  *dst_bit   src_strit  *drc_strit  (src_bt  *drc_bts	      src_x, src_y,dest_x,dest_y, width, heigh--;
    }

    return TRUE;
}#  idefiign		(GNUC__sk) &!defiign		(x86_64__sk) &!defiign		(amd64__s
	(at_stbusit_d ((c for28_alipy_g_po uiove_h)
#endif}

static pixman_bool_d
ssafill 8 (pixman_implementation_t *imp,
         t uint32 *               t_bits,,
         ui               nt     c_strits,,
         ui               nt     c_bts,,
         ui               nt     xts,,
         ui               nt     yts,,
         ui               nt     , widts,,
         ui               nt     , heigmp,
         t uint32 xorht)

	    if   pixmafilllt_sse2t_bit c_strit c_bt xt t_y, width, heig, xorh)se
    {
	retur_ (pixman_implementatiafill 8;

	  n_i->delegatag t_bit c_strit c_bt xt t_y, width, heig, xorh--;
    }

    return TRUE;
}

statit uint32 *_d
ssafetch_x8r8g8b8 8 (pixmaniov32 *niov,s  const uint32  (*maht)

	   ui   w niov->, widhi;
    __m1280xff00000=), mask_ff0000hi;     uint32_t *d w niov->bufferhi;     uint32_tm_smp  *(uint32_t niov->bit ms;

  niov->bit  +w niov->k_stride*/
    while (w && ((unsigned lon *dsk)== 0f)se
    {  *dst++ (= *src) | = 0xff0000++;
	w--;
    }

    while (w >= 4)
    {  save_128_aligned ()
   ((__m128i *)ds = _mm_or_si128 (
		load_128_unaligned ((__m12 8i*)src)0xff0000hi) }
  dst += 4
*)sst += 4
  w +=--;
    }

    while 5))
    {  *dst++ (= *src) | = 0xff0000++;
	w--;
    }

    retu niov->bufferUE;
}

statit uint32 *_d
ssafetch_r5g6b5 8 (pixmaniov32 *niov,s  const uint32  (*maht)

	   ui   w niov->, widhi;
    uint32_t *d w niov->bufferhi;     ui1632_tm_smp  *(ui1632_t niov->bit hi;
    __m1280xff00000=), mask_ff0000hi;

  niov->bit  +w niov->k_stride*/
    while (w && ((unsigned lon *dsk)== 0f)se
    {*(ui1632   s = *src++ {  *dst++yC8N_OVT_05658TO88_8888s)++;
	w--;
    }

    while (w >8 4)
    {
	__m128t_lotmp3s++ {smp = _m		louor_si12d ((__m12 8i*)sr++ {lomd = unpac5658to88_8888= _mm_unpacklo_epi16sm), _mm_setzero_si888)hi));himd = unpac5658to88_8888= _mm_unpahilo_epi16sm), _mm_setzero_si888)hi)) {  save_128_align2d ((__m12 8i(  dst== s = _mm_or_si128loc)0xff0000hi) {  save_128_align2d ((__m12 8i(  dst=4 s = _mm_or_si128tmp)0xff0000hi) }
  dst +8 4
*)sst +8 4
  w +8--;
    }

    while 5))
    {
	ui1632   s = *src++ {  *dst++yC8N_OVT_05658TO88_8888s)++;
	w--;
    }

    retu niov->bufferUE;
}

statit uint32 *_d
ssafetch_a8 8 (pixmaniov32 *niov,s  const uint32  (*maht)

	   ui   w niov->, widhi;
    uint32_t *d w niov->bufferhi;     ui832_tm_smp niov->bit hi
	    __m128i xmm0, xmm1, xmm2, xmm2, x4m2, x5m2, x6hi;

  niov->bit  +w niov->k_stride*/
    while (w &&  ((unsigned lon *dspd & 15))
              *dst++ *( *src) <<>> 24;
      
	w--;
    }

    while (w >165))
    {i xmmp = _m		louor_si1d ((__m12 8i*)sr++ {, xmsk = _mm_unpacklo_e8   , _mm_setzero_si128,   xmm0);  xmm2 = _mm_unpahilo_e8   , _mm_setzero_si128,   xmm0);  x3sk = _mm_unpacklo_epi  , _mm_setzero_si128,   x1m0);  x4m2 = _mm_unpahilo_epi  , _mm_setzero_si128,   x1m0);  x5sk = _mm_unpacklo_epi  , _mm_setzero_si128,   x2m0);  x6m2 = _mm_unpahilo_epi  , _mm_setzero_si128,   x2r++ {, _mmt coro_si12d ((__m12 8i(  dst=== 48), xmm3 {, _mmt coro_si12d ((__m12 8i(  dst==4 48), x4m3 {, _mmt coro_si12d ((__m12 8i(  dst==8 48), x5m3 {, _mmt coro_si12d ((__m12 8i(  dst=12 48), x6i) }
  dst >16 4
*)sst +16 4
  w -= --;
    }

    while 5))
    {  *dst++ *( *src) <<>> ++;
	w--;
    }

    retu niov->bufferUE;
}typedef c_suctt)
{
    pixmas.forme2_dage	s.form--;
   (pixmaniov3geest_scanlige	geest_scanliUE; fetchov3info_m--*/
stati  con fetchov3info_m fetchovs[] =c)

	      PIXMAx8r8g8b8,		d
ssafetch_x8r8g8b8 },

	      PIXM_r5g6b5,		d
ssafetch_r5g6b5 },

	      PIXM_a8,		d
ssafetch_a8 },

	      PIXM_n(fu    ;
}

static void
sse
	srctov3inib 8 (pixman_implementation_t *imp
		    (pixmaniov32 *niov,
				    pixman_image_tt_image,

	   ui x,  ui y,  uiy, widt  uih, heigge,

	    ui832_tbuffert  iov3flags32_flagsht)
#defiig FLAGS								\

	  (FAST_PATH_STANDARD_FLAGS | FAST_PATH_ID_TRANSN_FO)i;

    if(flagspd ITOV_NARROW)				&&
	(t_image2_cmon.flagspd FLAGS)rc  FLAGS		&&
	x(w >0(w &y(w >0				&&
		vxy, wid <= t_image->bit, wid			&&
	yvxy, heig <= t_image->bit, heig5))
    {  con fetchov3info_m *f;
}	s.f (f++ &fetchovs[0]; f->s.form(m    PIXM_n(fu; frc)ight--)
   i(t_image2_cmon.extendedas.forme2_darc  f->s.form(m)
	    {
  ui832_tbmp    ui832_t)t_image->bits.bi;),
 (2   s t_image->bits.rowstri *+=--),
niov->bit  s bvxys *+yvxyx *   PIXMAN_FORMAT_BPPf->s.form( /+8 4
	niov->k_strist = s;
niov->, wid w = width;
niov->buffermp  *(uint32_t buffer--),
niov->geest_scanlimp f->geest_scanlith;)
	retur	}
	    	}-;
    }

  _ (pixman_implementatie
	srctov3inib 8
	n_i->delegatag niov,st_imat xt t_y, width, heig, buffert_flagshUE;
}#  idefiign		(GNUC__sk) &!defiign		(x86_64__sk) &!defiign		(amd64__s
	(at_stbusit_d ((c for28_alipy_g_po uiove_h)
#endif} (pixman_implementation_t
_ (pixman_implementatiecreatelt_sse2 (pixman_implementation_tfallbacaht)

	   (pixman_implementation_t *imp _ (pixman_implementatiecreate (fallbaca, e_ssanarespaths)rn;

    /SSE2i  conriasry */
  (*mac5658r sk createmm_mas2x3228_128= 00f80000, = 00f80000)rn/
  (*mac5658g1sk createmm_mas2x3228_128= 00070000, = 00070000)rn/
  (*mac5658g2sk createmm_mas2x3228_128= 000000e0, = 000000e0)rn/
  (*mac5658b sk createmm_mas2x3228_128= 0000001f, = 0000001f)rn/
  (*macrgne sk createmm_mas2x3228_128= 00f80000, = 00f80000)rn/
  (*macgreensk createmm_mas2x3228_128= 0000fc00, = 0000fc00)rn/
  (*macblue sk createmm_mas2x3228_128= 000000f8, = 000000f8)rn/
  (*mac565an_f_rbsk createmm_mas2x3228_128= 00e000e0, = 00e000e0)rn/
  (*mac565an_f_gsk createmm_mas2x3228_1228= 0000c000, = 0000c000)rn/
  (*mac0080sk createmm_mas1628_128= 0080)rn/
  (*mac00ffsk createmm_mas1628_128= 00ff)rn/
  (*mac0101sk createmm_mas1628_128= 0101)rn/
  (*mask_ffsk createmm_mas1628_128= k_ff)rn/
  (*mask_ff0000sk createmm_mas2x3228_128= k_ff0000, = k_ff0000)rn/
  (*mand_alpsk createmm_mas2x3228_128= 00ff0000, = 00ff0000)rn;

    /Senstp functati po uiovsry */	  n_i->e_combin32[  PIXMAOB_8_OV] =  'sse2_combine_over;*/	  n_i->e_combin32[  PIXMAOB_8_OV_RE_OVSE] =  'sse2_combine_over_reverer;*/	  n_i->e_combin32[  PIXMAOB_IN] =  'sse2_combininer;*/	  n_i->e_combin32[  PIXMAOB_IN_RE_OVSE] =  'sse2_combininer_reverer;*/	  n_i->e_combin32[  PIXMAOB_OUT] =  'sse2_combineuter;*/	  n_i->e_combin32[  PIXMAOB_OUT_RE_OVSE] =  'sse2_combineuter_reverer;*/	  n_i->e_combin32[  PIXMAOB_ATOP] =  'sse2_combinatoper;*/	  n_i->e_combin32[  PIXMAOB_ATOP_RE_OVSE] =  'sse2_combinatoper_reverer;*/	  n_i->e_combin32[  PIXMAOB_XOV] =  'sse2_combinxover;*/	  n_i->e_combin32[  PIXMAOB_ADD] =  'sse2_combinadder;**/	  n_i->e_combin32[  PIXMAOB_SATURATE] =  'sse2_combinsaretatemr;**/	  n_i->e_combin3se2a[  PIXMAOB_SRC] =  'sse2_combinsrce2a;*/	  n_i->e_combin3se2a[  PIXMAOB_8_OV] =  'sse2_combine_ove2a;*/	  n_i->e_combin3se2a[  PIXMAOB_8_OV_RE_OVSE] =  'sse2_combine_over_revere2a;*/	  n_i->e_combin3se2a[  PIXMAOB_IN] =  'sse2_combinine2a;*/	  n_i->e_combin3se2a[  PIXMAOB_IN_RE_OVSE] =  'sse2_combininer_revere2a;*/	  n_i->e_combin3se2a[  PIXMAOB_OUT] =  'sse2_combineute2a;*/	  n_i->e_combin3se2a[  PIXMAOB_OUT_RE_OVSE] =  'sse2_combineuter_revere2a;*/	  n_i->e_combin3se2a[  PIXMAOB_ATOP] =  'sse2_combinatope2a;*/	  n_i->e_combin3se2a[  PIXMAOB_ATOP_RE_OVSE] =  'sse2_combinatoper_revere2a;*/	  n_i->e_combin3se2a[  PIXMAOB_XOV] =  'sse2_combinxove2a;*/	  n_i->e_combin3se2a[  PIXMAOB_ADD] =  'sse2_combinadde2a;**/	  n_i->n_b =  'ssen_b;*/	  n_i->fill =  'ssefill;**/	  n_i->
	srctov3inib =  'sse
	srctov3inib; }

    retu n_iUE;
