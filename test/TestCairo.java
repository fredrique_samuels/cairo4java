/**
 * Copyright (C) 2014 Fredique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.gamedev.cairo4j.CairoFormat;
import com.gamedev.cairo4j.CairoSurface;
import com.gamedev.cairo4j.cairo;


public class TestCairo {
	public static void main(String[] args) throws IOException {
		
		
		
		testImageLoading();
		testImageLoading2();
		{
			CairoSurface surface = cairo.cairo_image_surface_create(CairoFormat.CAIRO_FORMAT_ARGB32,
	            512, 512);
			cairo.cairo_surface_write_to_png(surface, "image_test.png");
			cairo.cairo_surface_destroy(surface);
		}
	}

	private static void testImageLoading() {
		CairoSurface image = cairo.cairo_image_surface_create_from_png("f3000_1.png");
		byte[] buffer = cairo.cairo_image_surface_get_data(image);
		int width = cairo.cairo_image_surface_get_width(image);
		int height = cairo.cairo_image_surface_get_height(image);
		int stride = cairo.cairo_image_surface_get_stride(image);
		System.out.println(width +  " " + height + " " + stride);
	
		
		CairoSurface image2 = cairo.cairo_image_surface_create_for_data(buffer, CairoFormat.CAIRO_FORMAT_ARGB32, width, height, stride);
		cairo.cairo_surface_write_to_png(image2, "image_test2.png");
		

		int width2 = cairo.cairo_image_surface_get_width(image2);
		int height2 = cairo.cairo_image_surface_get_height(image2);
		System.out.println(width2 +  " " + height2);
	}
	
	private static void testImageLoading2() throws IOException {
		FileInputStream fileInputStream = new FileInputStream("f3000_1.png");
		BufferedImage image = ImageIO.read(fileInputStream);
		CairoFormat format = CairoFormat.CAIRO_FORMAT_RGB24; 
		
		int width = image.getWidth();
		int height = image.getHeight();
		int bpp = 3;
		
		if (image.getColorModel().hasAlpha()) {
			bpp = 4;
			format = CairoFormat.CAIRO_FORMAT_ARGB32;
		}
		
		int[] pixels = new int[image.getWidth() * image.getHeight()];
        image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());
        
        byte[] buf = new byte[image.getWidth() * image.getHeight() * bpp];
        
        int index = 0;
        for(int y = 0; y < image.getHeight(); y++){
            for(int x = 0; x < image.getWidth(); x++){
                int pixel = pixels[y * image.getWidth() + x];
                buf[index++] = (byte) ((pixel >> 16) & 0xFF);     // Red component
                buf[index++] = (byte) ((pixel >> 8) & 0xFF);      // Green component
                buf[index++] = (byte) (pixel & 0xFF);               // Blue component
                if(image.getColorModel().hasAlpha()){
                	buf[index++] = (byte) ((pixel >> 24) & 0xFF);    // Alpha component. Only for RGBA
                }
            }
        }
	
		int stride = cairo.cairo_format_stride_for_width(format, width);
		
		CairoSurface image2 = cairo.cairo_image_surface_create_for_data(buf, CairoFormat.CAIRO_FORMAT_ARGB32, width, height, stride);
		
		cairo.cairo_surface_write_to_png(image2, "image_test3.png");
		fileInputStream.close();
	}
}
