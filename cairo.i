/* File : cairo.i */
%module(directors="1") cairo

%{
#include "c/cairo/src/cairo.h"
%}

cairo_status_t cairo_surface_write_to_png (cairo_surface_t*,const char*);

cairo_surface_t* cairo_image_surface_create_from_png (const char*);
    
/* Let's just grab the original header file here */
%include "c/cairo/src/cairo.h"



