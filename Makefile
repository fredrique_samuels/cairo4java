
EXT = 
JAVA_INC = 
CCFLAGS =

ifeq ($(OS),Windows_NT)
	CCFLAGS += -Wl,--kill-at -D WIN32
    JAVA_INC = -I"C:\Program Files (x86)\Java\jdk1.6.0_21\include" -I"C:/Program Files (x86)/Java/jdk1.6.0_21/include/win32"
	EXT=.dll 
	ifeq ($(PROCESSOR_ARCHITECTURE),AMD64)
		CCFLAGS += -D AMD64
	endif
	ifeq ($(PROCESSOR_ARCHITECTURE),x86)
		CCFLAGS += -D IA32
	endif
else
	UNAME_S := $(shell uname -s)
    JAVA_INC = -I/usr/lib/jvm/jdk1.6/include -I/usr/lib/jvm/jdk1.6/include/linux
	ifeq ($(UNAME_S),Linux)
		CCFLAGS += -D LINUX -fPIC
		EXT=.so 
	endif
endif


LIBS	   = -lm
CC=gcc
LINK=gcc
CFLAGS=-O2 -fmessage-length=0 -fpermissive
LDFLAGS=

LIBCAIRO4J_SRC = cairo_wrap.c


LIBCAIRO4J_OBJ = $(LIBCAIRO4J_SRC:.c=.o)
LIBCAIRO4J=libcairo4j$(EXT)
LIBCAIRO4J_CFLAGS =-DCAIRO_HAS_PNG_FUNCTIONS -D_USE_MATH_DEFINES -DPIXMAN_NO_TLS -DPACKAGE="android-cairo" -DUSE_ARM_NEON -DUSE_ARM_SIMD
LOCAL_CFLAGS = $(CCFLAGS) -Ic/cairo/src -Ic/cairo/src -Ic/cairo/cairo-extra -Wno-missing-field-initializers $(JAVA_INC)
 		

all: $(LIBCAIRO4J_SRC) $(LIBCAIRO4J)

# real package dir is src/com/gamedev/cairo4j

$(LIBCAIRO4J): $(LIBCAIRO4J_OBJ)
	#swig -java -package com.gamedev.cairo4j -outdir temp cairo.i
	$(LINK) $(CCFLAGS) -DCAIRO_HAS_PNG_FUNCTIONS -shared -o $@ $(LIBCAIRO4J_OBJ) -Llpng1413 -Lzlib114 -Lc/cairo  -Lc/pixman  -lcairo  -lpixman -lpng -lz >> debug.txt

.c.o:
	$(CC) -c -DCAIRO_HAS_PNG_FUNCTIONS $(MACROS) $(LOCAL_CFLAGS) $(INC_DIRS) $(LIBCAIRO4J_CFLAGS) $< -o $@

clean:
	rm -r $(LIBCAIRO4J_OBJ)
