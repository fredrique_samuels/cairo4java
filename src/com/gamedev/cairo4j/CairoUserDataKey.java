/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.10
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.gamedev.cairo4j;

public class CairoUserDataKey {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected CairoUserDataKey(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(CairoUserDataKey obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        cairoJNI.delete_cairo_user_data_key_t(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public void setUnused(int value) {
    cairoJNI.cairo_user_data_key_t_unused_set(swigCPtr, this, value);
  }

  public int getUnused() {
    return cairoJNI.cairo_user_data_key_t_unused_get(swigCPtr, this);
  }

  public CairoUserDataKey() {
    this(cairoJNI.new_cairo_user_data_key_t(), true);
  }

}
