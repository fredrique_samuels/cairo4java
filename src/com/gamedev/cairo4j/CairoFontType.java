/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.10
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.gamedev.cairo4j;

public final class CairoFontType {
  public final static CairoFontType CAIRO_FONT_TYPE_TOY = new CairoFontType("CAIRO_FONT_TYPE_TOY");
  public final static CairoFontType CAIRO_FONT_TYPE_FT = new CairoFontType("CAIRO_FONT_TYPE_FT");
  public final static CairoFontType CAIRO_FONT_TYPE_WIN32 = new CairoFontType("CAIRO_FONT_TYPE_WIN32");
  public final static CairoFontType CAIRO_FONT_TYPE_QUARTZ = new CairoFontType("CAIRO_FONT_TYPE_QUARTZ");
  public final static CairoFontType CAIRO_FONT_TYPE_USER = new CairoFontType("CAIRO_FONT_TYPE_USER");

  public final int swigValue() {
    return swigValue;
  }

  public String toString() {
    return swigName;
  }

  public static CairoFontType swigToEnum(int swigValue) {
    if (swigValue < swigValues.length && swigValue >= 0 && swigValues[swigValue].swigValue == swigValue)
      return swigValues[swigValue];
    for (int i = 0; i < swigValues.length; i++)
      if (swigValues[i].swigValue == swigValue)
        return swigValues[i];
    throw new IllegalArgumentException("No enum " + CairoFontType.class + " with value " + swigValue);
  }

  private CairoFontType(String swigName) {
    this.swigName = swigName;
    this.swigValue = swigNext++;
  }

  private CairoFontType(String swigName, int swigValue) {
    this.swigName = swigName;
    this.swigValue = swigValue;
    swigNext = swigValue+1;
  }

  private CairoFontType(String swigName, CairoFontType swigEnum) {
    this.swigName = swigName;
    this.swigValue = swigEnum.swigValue;
    swigNext = this.swigValue+1;
  }

  private static CairoFontType[] swigValues = { CAIRO_FONT_TYPE_TOY, CAIRO_FONT_TYPE_FT, CAIRO_FONT_TYPE_WIN32, CAIRO_FONT_TYPE_QUARTZ, CAIRO_FONT_TYPE_USER };
  private static int swigNext = 0;
  private final int swigValue;
  private final String swigName;
}

