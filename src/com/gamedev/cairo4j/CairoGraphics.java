/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.10
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.gamedev.cairo4j;

public class CairoGraphics {
  private long swigCPtr;

  protected CairoGraphics(long cPtr, boolean futureUse) {
    swigCPtr = cPtr;
  }

  protected CairoGraphics() {
    swigCPtr = 0;
  }

  protected static long getCPtr(CairoGraphics obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }
}

